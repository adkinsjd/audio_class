/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

public class NoiseTracker {
	final int nCh;
	final double minVal;
	final int indicator_countdown;
	final double floor_up;
	final double floor_up_slow;
	final double floor_down;

	double[] floor_thresh;
	int[] indicator_last;
	
	public NoiseTracker(int nCh, double minVal, int sigDur){
		this.nCh = nCh;
		this.minVal = minVal;
		indicator_countdown = sigDur;
		double alpha_updown = 0.01;
		double slow_scale = 0.5;
		floor_up = 1+alpha_updown;
		floor_up_slow = 1+slow_scale*alpha_updown;
		floor_down = 1-alpha_updown;
		
		floor_thresh = new double[nCh];
		indicator_last = new int[nCh]; // default to 0
		for (int i = 0; i < nCh; i++){
			floor_thresh[i] = minVal;
			indicator_last[i] = indicator_countdown;
		}
	}
	
	public void update(double[] frame, int j){
		if (frame[j] > floor_thresh[j]){
			indicator_last[j] -= 1;
			if (indicator_last[j] < 0){ // noise period
				floor_thresh[j] = floor_thresh[j]*floor_up;
			}
			else{ // signal period
				floor_thresh[j] = floor_thresh[j]*floor_up_slow;
			}
		}
		else{ // noise period
	        floor_thresh[j] = Math.max(minVal, floor_thresh[j]*floor_down);
	        indicator_last[j] = indicator_countdown;
		}
	}
}
