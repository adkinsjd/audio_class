% Prepare GCWA_TFRHMM for deployment
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;

%rootDir = 'C:/cygwin64/home/BLISSBOX/';
%rootDir = 'C:/cygwin64/home/UyenBui/';
rootDir = '/home/blissbox/';
%rootDir = 'C:/Users/Long/Projects/';

%rootDatDir = 'D:/GradStudents/longle1/';
rootDatDir = '/media/blissbox/DATAPART1/GradStudents/longle1/';
%rootDatDir = [rootDir 'data/'];

addpath([rootDir 'voicebox/'])
addpath([rootDir 'cohmm/'])
addpath([rootDir 'node-paper/'])
addpath([rootDir 'jsonlab/'])
addpath([rootDir 'V1_1_urlread2'])
addpath([rootDir 'sas-clientLib/matlab/src'])

servAddr = 'acoustic.ifp.illinois.edu:8080';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
DATA = 'data';
EVENT = 'event';

%% train model using data or load hand-tuned model
save('./GCWA_TFRHMM.mat','cohmm1','cohmm2');

%% test before compile
% manual label data
clear q;
%q.t1 = datenum(2016,03,18,07,05,00); q.t2 = datenum(2016,03,18,07,58,00);
q.t1 = datenum(2016,03,17,00,00,00); q.t2 = datenum(2016,03,19,00,00,00);
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);
events = interactiveTag(events, 1, servAddr, DB, USER, PWD, DATA, EVENT);
%save('localLogs/events.mat','events');

isGCWA = false(1,numel(events));
for k =1:numel(events)
    if isfield(events{k},'tag') && strcmp(events{k}.tag,'GCWA')
        isGCWA(k) = true;
    end
end

% test
fid = fopen('dataList','w+');
fileSubset = 901:1100;%1:numel(events);
for k =fileSubset(1:end-1)
    fprintf(fid,'%s\n',events{k}.filename);
end
fprintf(fid,'%s',events{fileSubset(end)}.filename);
fclose(fid);

nLogProb = GCWA_TFRHMM(servAddr,DB,PWD);

figure;
ax(1) = subplot(311); plot(exp(nLogProb))
ax(2) = subplot(312); plot(nLogProb)
ax(3) = subplot(313); stem(isGCWA(fileSubset))
linkaxes(ax,'x')

%% test after compile
% upload local data to service
fileList = {'hello.wav',...
            'GCWA_col1_ele1.wav',...
            'GCWA_col1_ele2.wav',...
            'GCWA_col1_ele3.wav',...
            'GCWA_col1_ele4.wav',...
            'GCWA_col1_ele5.wav',...
            'GCWA_col2_ele1.wav',...
            'GCWA_col2_ele2.wav',...
            'GCWA_col3_ele1.wav',...
            'GCWA_col3_ele2.wav',...
            'GCWA_col3_ele3.wav',...
            'GCWA_col3_ele4.wav',...
            'GCWA_col3_ele5.wav',...
            'GCWA_col3_ele6.wav',...
            'GCWA_col3_ele7.wav',...
            'GCWA_col3_ele8.wav',...
            'GCWA_col3_ele9.wav',...
            'GCWA_col3_ele10.wav',...
            'GCWA_col3_ele11.wav',...
            'GCWA_col3_ele12.wav',...
            'GCWA_col4_ele1.wav',...
            };
%{
% play the sound files
for k =2:8%numel(fileList)
    fprintf(1,'k = %d\n',k);
    [y,fs] = audioread([rootDatDir fileList{k}]);
    playblocking(audioplayer(y,fs));
    pause(2);
end
fname = '20160319052051384.wav';
resp = IllColDelete(servAddr, DB, USER, PWD, EVENT, fname);
resp = IllGridDelete(servAddr, DB, USER, PWD, DATA, fname);
%}
if false
    for k =1:numel(fileList)
        fprintf(1,'k = %d\n',k);
        aEvent.filename = fileList{k};
        aEvent.key = PWD;
        status = IllColPost(servAddr, DB, USER, PWD, EVENT, aEvent);
        fid = fopen([rootDatDir fileList{k}],'r');
        data = fread(fid,'*char');
        fclose(fid);
        resp = IllGridPost(servAddr, DB, USER, PWD, DATA, fileList{k}, data);
        pause(1);
    end
end
%events = IllColGet(servAddr,DB, USER, PWD, EVENT, fileList{7});

% test
fid = fopen('dataList','w+');
for k =1:numel(fileList)-1
    fprintf(fid,'%s\n',fileList{k});
end
fprintf(fid,'%s',fileList{end});
fclose(fid);
% output from matlab
GCWA_TFRHMM(servAddr,DB,PWD);

fid = fopen('dataList','w+');
for k =1:numel(fileList)-1
    fprintf(fid,'%s\n',fileList{k});
end
fprintf(fid,'%s',fileList{end});
fclose(fid);
% output from mex file
mcc -m -R -nodisplay -v GCWA_TFRHMM.m % compile
system(sprintf('./run_GCWA_TFRHMM.sh /usr/local/MATLAB/MATLAB_Runtime/v85/ %s %s %s', servAddr,DB,PWD));

%% install
installDir = [rootDir 'sas/libs/models/'];
if ~exist('GCWA_TFRHMM', 'dir')
    mkdir(installDir,'GCWA_TFRHMM');
end
copyfile('./run_GCWA_TFRHMM.sh', [installDir 'GCWA_TFRHMM/'])
copyfile('./GCWA_TFRHMM', [installDir 'GCWA_TFRHMM/'])
copyfile('./GCWA_TFRHMM.mat', [installDir 'GCWA_TFRHMM/'])
