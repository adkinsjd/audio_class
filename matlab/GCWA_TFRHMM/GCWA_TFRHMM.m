function nLogProb = GCWA_TFRHMM(servAddr,DB,PWD)
% prob = GCWA_TFRHMM(servAddr,DB,PWD)
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
dataList = './dataList';
DATA = 'data';
EVENT = 'event';

% load the model
load('./GCWA_TFRHMM.mat','cohmm1','cohmm2');

% read the data list
nLogProb = zeros(0,1);
fid = fopen(dataList,'r+');
if fid > 0
    cnt = 0;
    while true
        fname = fgetl(fid);
        if fname  == -1
            break;
        end
        cnt = cnt+1;
        nLogProb(cnt) = 0;

        events = IllColGet(servAddr,DB, 'nan', PWD, EVENT, fname);
        if ~iscell(events)
            fprintf(2,'No event structure, skipped this file\n');
            continue;
        else
            if isfield(events{1},'blkSize') && isfield(events{1},'incSize')
                nBlk = events{1}.blkSize;
                nInc = events{1}.incSize;
            else
                nBlk = 512;
                nInc = nBlk/2;
            end
            
            if isfield(events{1},'TFRidgeFeat')
                tStr = fieldnames(events{1}.TFRidgeFeat);
                tIdx = zeros(1,numel(tStr));
                fIdx = cell(1,numel(tStr));
                for k = 1:numel(tStr)
                    tIdx(k) = sscanf(tStr{k},'t%d');
                    fIdx{k} = events{1}.TFRidgeFeat.(tStr{k});
                end
                fs = events{1}.fs;
                
                % size = 1st full block + incremental blocks
                obs = cell(1,fix((0.8+events{1}.maxDur)*fs/nInc)-1); % assume lag of sensor is 2 x 0.4 s
                tBaseIdx = fix(0.4*fs/nInc);
                btLenIdx = fix(0.064*fs/nInc); % assume btLen of sensor is 0.064 s
                for k = 1:numel(tIdx)
                    obs{tBaseIdx+tIdx(k)-btLenIdx} = fIdx{k}+1; % sensor freq idx starts from 0
                end
                %{
                for k = 1:numel(obs)
                    if numel(obs{k}) == 0
                        obs{k} = 1;
                    end
                end
                %}
            else
                data = IllGridGet(servAddr, DB, 'nan', PWD, DATA, fname);
                try
                    [y, header] = wavread_char(data);
                catch
                    fprintf(2,'No feat and data, skipped this file\n');
                    continue;
                end
                fs = double(header.sampleRate);
                [~,specDet] = detectOneFile([],y,fs,nBlk,nInc,0);
                obs = cell(1,size(specDet,2));
                for  k = 1:numel(obs)
                    obs{k} = find(specDet(:,k) > 0);
                    %{
                    if numel(obs{k}) == 0
                        obs{k} = 1;
                    end
                    %}
                end
            end
            [estStates1] = cohmmViterbiObsCtl(cohmm1,obs);
            nLogProb1 = cohmmEvalObsCtl(cohmm1,obs,estStates1,0);
            [estStates2] = cohmmViterbiObsCtl(cohmm2,obs);
            nLogProb2 = cohmmEvalObsCtl(cohmm2,obs,estStates2,0);
            nLogProb(cnt) = max(nLogProb1,nLogProb2);
        end
    end
    fclose(fid);
end

%fprintf(1,'[');
if numel(nLogProb) > 0
    str = sprintf('%f,',exp(nLogProb));
    fprintf(1,'%s',str(1:end-1));
end
%fprintf(1,']\n');

% clean the dataList after read
fid = fopen(dataList,'w');
fclose(fid);

end