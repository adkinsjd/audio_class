function [score,allPos,C,V,lnk] = tempMatch(temp,data,mode)
    %
    %
    % Long Le <longle1@illinois.edu>
    % University of Illinois
    %   
    n1 = size(temp,2);
    n2 = size(data,2);

    radT = 5;
    C = zeros(n1,n2);
    for k = 1:n1
        for l = 1:n2
            x1 = mean(temp(:,max(1,k-radT):min(n1,k+radT)),2);
            x2 = mean(data(:,max(1,l-radT):min(n2,l+radT)),2);
            if x1'*x2 + (1-x1')*x2 == 0
                C(k,l) = 0;
            else
                if (norm(x1) ~= 0 && norm(x2) ~= 0)
                    C(k,l) = x1'*x2/norm(x2)/norm(x1);
                end
            end
        end
    end

    V = -ones(n1,n2);
    lnk = nan(n1,n2);
    params.n1 = size(V,1);
    params.n2 = size(V,2);
    params.slope = size(V,2)/size(V,1);
    params.offset = 20;

    src = [n1,n2];
    dst = [1,1];
    if mode == 1
        [V,lnk] = dfs(C,src,V,lnk,params);
        allPos = fTracer(lnk,src,dst);
        score = V(src(1),src(2));
    else
        [V,lnk] = bfs(C,src,V,lnk,params);
        allPos = bTracer(lnk,src,dst);
        score = V(dst(1),dst(2));
    end
end

function [V,lnk] = bfs(C,src,V,lnk,params)
    %Q = posQueue;
    buf = zeros(0,2);
    cnt = 0;
    
    %Q = Q.enqueue(src);
    cnt = cnt + 1;
    buf(cnt,:) = src;
    V(src(1),src(2)) = C(src(1),src(2));
    %lnk(pos(1),pos(2)) = 1;
    
    %while ~Q.isEmpty()
    while cnt > 0
        %[Q,curPos] = Q.dequeue();
        curPos = buf(1,:);
        buf(1,:) = [];
        cnt = cnt-1;

        nghPos = neighbor(curPos,params);
        for k = 1:length(nghPos)
            if ~isnan(nghPos{k})
                val = C(nghPos{k}(1),nghPos{k}(2)) + V(curPos(1),curPos(2));
                if val > V(nghPos{k}(1),nghPos{k}(2))
                    V(nghPos{k}(1),nghPos{k}(2)) = val;
                    lnk(nghPos{k}(1),nghPos{k}(2)) = k;
                    
                    %Q = Q.enqueue(nghPos{k});
                    cnt = cnt + 1;
                    buf(cnt,:) = nghPos{k};
                end
            end
        end
        
        % early termination
        %{
        lastCol = max(buf(:,2))+1;
        if  lastCol < size(V,2)-10
            tV = V(:,lastCol);
            tV(tV<0) = nan;
            if nanmean(tV) < 50
                break;
            end
        end
        %}
    end
end

function neighborPos = neighbor(pos,params)
    neighborPos = {nan,nan,nan};
    
    if isConMet(pos-[1,1],params)
        neighborPos{1} = pos-[1,1];
    end
    if isConMet(pos-[1,0],params)
        neighborPos{2} = pos-[1,0];
    end
    if isConMet(pos-[0,1],params)
        neighborPos{3} = pos-[0,1];
    end
end

function allPos = bTracer(lnk,src,dst)
    allPos = zeros(0,2);
    cnt = 0;
    pos = [dst(1),dst(2)];
    while pos(1) ~= src(1) || pos(2) ~= src(2)
        cnt = cnt + 1;
        allPos(cnt,:) = pos;
        
        if lnk(pos(1),pos(2)) == 1
            pos = pos + [1,1];
        elseif lnk(pos(1),pos(2)) == 2
            pos = pos + [1,0];
        elseif lnk(pos(1),pos(2)) == 3
            pos = pos + [0,1];
        else
            break;
        end
    end
    cnt = cnt + 1;
    allPos(cnt,:) = pos;
end

function [V,lnk] = dfs(C,pos,V,lnk,params)
    if isConMet(pos,params)
        nextVals = -ones(1,3);
        % if in neighborhood
        if isConMet(pos-[1,1],params)
            if V(pos(1)-1,pos(2)-1) == -1
                [V,lnk] = dfs(C,pos-[1,1],V,lnk,params);
            end
            nextVals(1) = V(pos(1)-1,pos(2)-1);
        end
        if isConMet(pos-[1,0],params)
            if V(pos(1)-1,pos(2)) == -1
                [V,lnk] = dfs(C,pos-[1,0],V,lnk,params);
            end
            nextVals(2) = V(pos(1)-1,pos(2));
        end
        if isConMet(pos-[0,1],params)
            if V(pos(1),pos(2)-1) == -1
                [V,lnk] = dfs(C,pos-[0,1],V,lnk,params);
            end
            nextVals(3) = V(pos(1),pos(2)-1);
        end
    
        if sum(nextVals == -1) == 3
            V(pos(1),pos(2)) = C(pos(1),pos(2));
            %lnk(pos(1),pos(2)) = 1; % must exit the matrix/maze
        else
            [val,idx] = max(nextVals);
            V(pos(1),pos(2)) = C(pos(1),pos(2))+val;
            lnk(pos(1),pos(2)) = idx;
        end
    end
end

function status = isConMet(pos,params)
    slope = params.slope;
    offset = params.offset;
    n1 = params.n1;
    n2 = params.n2;
    x = pos(1);
    y = pos(2);
    
    status = (y <= slope*x+offset) &&...
            (y >= slope*x-offset) &&...
            (x >= 1) &&...
            (x <= n1) &&...
            (y >= 1) &&...
            (y <= n2);
end

function allPos = fTracer(lnk,src,dst)
    allPos = zeros(0,2);
    cnt = 0;
    pos = src;
    while pos(1) ~= dst(1) || pos(2) ~= dst(2)
        cnt = cnt + 1;
        allPos(cnt,:) = pos;
        
        if lnk(pos(1),pos(2)) == 1
            pos = pos - [1,1];
        elseif lnk(pos(1),pos(2)) == 2
            pos = pos - [1,0];
        elseif lnk(pos(1),pos(2)) == 3
            pos = pos - [0,1];
        else
            break;
        end
    end
end

