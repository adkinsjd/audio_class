% Analyze a GCWA call using various techniques
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all;close all;

%rootDir = 'C:/cygwin64/home/BLISSBOX/';
rootDir = '/home/blissbox/';
%rootDir = 'C:/cygwin64/home/UyenBui/';
%rootDir = 'C:/Users/Long/Projects/';

%rootDatDir = 'D:/GradStudents/longle1/data/';
rootDatDir = [rootDir 'gcwa/data/'];

addpath([rootDir 'voicebox/'])
addpath([rootDir 'cohmm/'])
addpath([rootDir 'sp_sensor/'])
addpath([rootDir 'jsonlab/'])
addpath([rootDir 'V1_1_urlread2/'])
addpath([rootDir 'sas-client/matlab/src/'])

servAddr = 'acoustic.ifp.illinois.edu:8080';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
DATA = 'data';
EVENT = 'event';

%% data for manual model
%[y,fs] = audioread(sprintf([rootDatDir 'GCW-A-(17).wav'])); y = resample(y,24e3,fs); fs = 24e3;
%[y,fs] = audioread(sprintf([rootDatDir 'hello.wav']));
[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col1_ele1.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col1_ele2.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col1_ele3.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col1_ele4.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col1_ele5.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col2_ele1.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col2_ele2.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele1.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele2.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele3.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele4.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele5.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele6.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele7.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele8.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele9.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele10.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele11.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col3_ele12.wav']));
%[y,fs] = audioread(sprintf([rootDatDir 'GCWA_col4_ele1.wav']));
%{
aEvent = events{1018}; data = IllGridGet(servAddr, DB, 'nan', PWD, DATA, aEvent.filename);
try
    [y, header] = wavread_char(data);
catch
    fprintf(2,'No feat and data, skipped this file\n');
end
fs = double(header.sampleRate);
%}

eps = 1e-6;
colStr = 'krgbcmykrgbcmy';
nBlk = 512;
nFc = nBlk/2;
nInc = 256;
[S,tt,ff] = mSpectrogram(y,fs,nBlk,nInc);
reCompute = true;

if reCompute
    % compute feature from raw data
    [newState,specDet] = detectOneFile([],y,fs,nBlk,nInc,1);
    obs = cell(1,size(specDet,2));
    for  k = 1:numel(obs)
        obs{k} = find(specDet(:,k) > 0);
        if numel(obs{k}) == 0
            obs{k} = 1;
        end
    end
else
    % use precomputed feature
    tStr = fieldnames(aEvent.TFRidgeFeat);
    tIdx = zeros(1,numel(tStr));
    fIdx = cell(1,numel(tStr));
    for k = 1:numel(tStr)
        tIdx(k) = sscanf(tStr{k},'t%d');
        fIdx{k} = aEvent.TFRidgeFeat.(tStr{k});
    end
    fs = aEvent.fs;

    % size = 1st full block + incremental blocks
    obs = cell(1,fix((0.8+aEvent.maxDur)*fs/nInc)-1); % assume lag of sensor is 2 x 0.4 s
    tBaseIdx = fix(0.4*fs/nInc);
    btLenIdx = fix(0.064*fs/nInc); % assume btLen of sensor is 0.064 s
    for k = 1:numel(tIdx)
        obs{tBaseIdx+tIdx(k)-btLenIdx} = fIdx{k}+1; % sensor freq idx starts from 0
    end
    %{
    for k = 1:numel(obs)
        if numel(obs{k}) == 0
            obs{k} = 1;
        end
    end
    %}
    specDet = zeros(nFc,numel(obs));
    for k = 1:numel(obs)
        specDet(obs{k},k) = 1;
    end
end
    
%% setup manual model 1
cohmm1.pi = [1;0;0;0;0;0;0;0];

p(1) = 1-1/(0.300*fs/nInc); % out-of-signal silence
p(2) = 1-1/(0.060*fs/nInc);
p(3) = 1-1/(0.060*fs/nInc);
p(4) = 1-1/(0.030*fs/nInc);
p(5) = 1-1/(0.300*fs/nInc);
p(6) = 1-1/(0.190*fs/nInc);
cohmm1.A = [p(1) 1-p(1) 0 0 0 0 0 0; 
            0 p(2) 1-p(2) 0 0 0 0 0; 
            0 0 p(3) (1-p(3))/2 (1-p(3))/2 0 0 0;
            0 (1-p(4))/2 0 p(4) (1-p(4))/2 0 0 0;
            0 0 0 0 p(5) (1-p(5))/2 (1-p(5))/2 0; % allow silent state skipping
            0 0 0 0 0 p(4) 1-p(4) 0;
            0 0 0 0 0 0 p(6) 1-p(6);
            0 0 0 0 0 0 0 1]; % having strictly 0 transition avoid early passing through the chain
for k = 1:size(cohmm1.A,1)
    cohmm1.A(k,:) = cohmm1.A(k,:) ./ sum(cohmm1.A(k,:));
end

clear h H;
H(1,:) = eps*ones(1,nFc*2); % in-signal silence
h{2} = fir1(128, [5291 5764]/(fs/2)); H(2,:) = abs(fft(h{2},nFc*2)); % mini chirp up
h{3} = fir1(128, [4724 5291]/(fs/2)); H(3,:) = abs(fft(h{3},nFc*2)); % mini chirp down
h{4} = fir1(64, [4819 6425]/(fs/2)); H(4,:) = abs(fft(h{4},nFc*2)); % middle chirp
h{5} = fir1(64, [6839 7655]/(fs/2)); H(5,:) = abs(fft(h{5},nFc*2)); % downward chirp
H(:,nFc+1:end) = [];
%{
figure;
for k = 1:size(H,1)
    subplot(size(H,1),1,k); plot([0:nFc]/nFc*fs/2,H(k,:))
end
%}
cohmm1.B = [H(1,:);
            H(2,:);
            H(3,:);
            H(1,:);
            H(4,:);
            H(1,:);
            H(5,:);
            H(1,:)];
maxVal = max(cohmm1.B(:));
for k = 1:size(cohmm1.B,1)
    cohmm1.B(k,:) = cohmm1.B(k,:) ./ maxVal;
end
cohmm1.B0 = [0.99;
             min(cohmm1.B(2,:));
             min(cohmm1.B(3,:));
             0.99;
             min(cohmm1.B(4,:));
             0.99;
             min(cohmm1.B(5,:));
             0.99];

% Apply model to data
[estStates,logProb] = cohmmViterbiObsCtl(cohmm1,obs);
norm( logProb/numel(obs)-cohmmEvalObsCtl(cohmm1,obs,estStates,1) )
nLogProb = cohmmEvalObsCtl(cohmm1,obs,estStates,0);

figure;
ax(1) = subplot(221); imagesc(tt,ff,specDet); axis xy;
ax(2) = subplot(222); hold on;
for k = 1:size(cohmm1.B,1)
	plot(cohmm1.B(k,:),[0:nFc-1]/nFc*fs/2,colStr(k));
end
ax(3) = subplot(223); hold on; plot(tt,estStates); axis tight;
plot(tt,1*ones(1,numel(tt)),'r:')
plot(tt,4*ones(1,numel(tt)),'r:')
plot(tt,6*ones(1,numel(tt)),'r:')
plot(tt,8*ones(1,numel(tt)),'r:')
suptitle(sprintf('nLogProb = %.6f',nLogProb))
linkaxes(ax(1:2),'y');
linkaxes(ax(1:2:3),'x');

%% setup manual model 2
cohmm2.pi = [1;0;0;0;0;0];

p(1) = 1-1/(0.300*fs/nInc); % out-of-signal silence
p(2) = 1-1/(0.184*fs/nInc);
p(3) = 1-1/(0.050*fs/nInc); 
p(4) = 1-1/(0.300*fs/nInc); 
p(5) = 1-1/(0.225*fs/nInc); 
cohmm2.A = [p(1) 1-p(1) 0 0 0 0;
            0 p(2) (1-p(2))/2 (1-p(2))/2 0 0;
            0 (1-p(3))/2 p(3) (1-p(3))/2 0 0;
            0 0 0 p(4) 1-p(4) 0;
            0 0 0 0 p(5) 1-p(5);
            0 0 0 0 0 1];
for k = 1:size(cohmm2.A,1)
    cohmm2.A(k,:) = cohmm2.A(k,:) ./ sum(cohmm2.A(k,:));
end

clear h H;
H(1,:) = eps*ones(1,nFc*2); % in-signal silence
h{2} = fir1(128, [3969 4441]/(fs/2)); H(2,:) = abs(fft(h{2},nFc*2)); % mini chirp
h{3} = fir1(64, [4252 5858]/(fs/2)); H(3,:) = abs(fft(h{3},nFc*2)); % middle chirp
h{4} = fir1(128, [6142 6520]/(fs/2)); H(4,:) = abs(fft(h{4},nFc*2)); % final chirp
H(:,nFc+1:end) = [];
%{
figure;
for k = 1:size(H,1)
    subplot(size(H,1),1,k); plot([0:nFc]/nFc*fs/2,H(k,:))
end
%}
cohmm2.B = [H(1,:);
            H(2,:);
            H(1,:);
            H(3,:);
            H(4,:);
            H(1,:)];
maxVal = max(cohmm2.B(:));
for k = 1:size(cohmm2.B,1)
    cohmm2.B(k,:) = cohmm2.B(k,:) ./ maxVal;
end
cohmm2.B0 = [0.99;
             min(cohmm2.B(2,:));
             0.99;
             min(cohmm2.B(3,:));
             min(cohmm2.B(4,:));
             0.99];

% Apply model to data
[estStates,logProb] = cohmmViterbiObsCtl(cohmm2,obs);
norm( logProb/numel(obs)-cohmmEvalObsCtl(cohmm2,obs,estStates,1) )
nLogProb = cohmmEvalObsCtl(cohmm2,obs,estStates,0);

figure;
ax(1) = subplot(221); imagesc(tt,ff,specDet); axis xy;
ax(2) = subplot(222); hold on;
for k = 1:size(cohmm2.B,1)
	plot(cohmm2.B(k,:),[0:nFc-1]/nFc*fs/2,colStr(k));
end
%set(gca,'Xdir','reverse');
%axis tight
ax(3) = subplot(223); hold on; plot(tt,estStates); axis tight;
plot(tt,1*ones(1,numel(tt)),'r:')
plot(tt,3*ones(1,numel(tt)),'r:')
plot(tt,6*ones(1,numel(tt)),'r:')
suptitle(sprintf('nLogProb = %.6f',nLogProb))
linkaxes(ax(1:2),'y');
linkaxes(ax(1:2:3),'x');

%% frequency analysis
q.t1 = datenum(2016,03,09,18,41,00); q.t2 = datenum(2016,03,09,19,01,00);
q.tag = 'GCWA';
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);

nFile = numel(events);
all = cell(nFile,1);
for l = 1:nFile
    data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{l}.filename);
    try
        [y, header] = wavread_char(data);
        fs = double(header.sampleRate);
    catch e
        disp('missing binary data')
        continue;
    end
    
    [newState,specDet] = detectOneFile([],y,fs,256,64,0);
    obs = cell(1,size(specDet,2));
    for  k = 1:size(specDet,2)
        obs{k} = find(specDet(:,k) > 0);
    end
end

%% cepstrum analysis
nFile = 17;
blockSize = 512;
d = 16;
allCepstCoef = cell(nFile,1);
allZ = cell(nFile,1);
for l = 1:nFile
    %[y,fs] = audioread(sprintf('D:/GradStudents/longle1/data/GCW-A-(%d).wav',l));
    %[y,fs] = audioread(sprintf('C:/Users/Long/Projects/network-paper/genCascade/data/GCW/GCW-A-(%d).wav',l));
    [y,fs] = audioread(sprintf('C:/cygwin64/home/UyenBui/data/GCW-A-(%d).wav',l));
    % y = resample(y,16e3,fs); fs = 16e3;
    % [S,tt,ff] = mSpectrogram(y,fs,256,16);
    b = fir1(32,[3.5e3/(fs/2) 8.5e3/(fs/2)]);
    y = filter(b,1,y);
    
    nBank = floor(3*log(fs));
    h1=melbankm(nBank,blockSize,fs,0,0.5,'tz');
    Y=enframe(y,hamming(blockSize),blockSize/2)'; % 'M' hamming
    S = zeros(blockSize/2+1,size(Y,2));
    C = zeros(d,size(Y,2));
    Z = zeros(nBank,size(Y,2));
    for k = 1:size(Y,2)
        %f=rfft(Y(:,k));
        allF = fft(Y(:,k));
        f = allF(1:blockSize/2+1);
        S(:,k) = abs(f);

        z=log(h1*abs(f)); % 'a' amplitude
        %z=log(h*abs(f).^2); % 'p' power
        Z(:,k) = z;

        c=dct(z)'; % nBank x 1
        C(:,k) = c(2:d+1);
    end
    cepstCoef = melcepst(y,fs,'Mtaz',16,nBank,blockSize);
    allCepstCoef{l} = cepstCoef;
    norm( C-cepstCoef' )
    allZ{l} = Z;
end

%% PCA analysis
allPcaScore = cell(nFile,1);
%{ 
%Group analysis
[pcaCoeff,pcaScore,pcaLatent] = pca(cell2mat(allCepstCoef));
mAllCepstCoef = mean(cell2mat(allCepstCoef),1);
for l = 1:nFile
    allPcaScore{l} = (allCepstCoef{l} - repmat(mAllCepstCoef,size(allCepstCoef{l},1),1))/(pcaCoeff');
end
norm( cell2mat(allPcaScore) - pcaScore )
%}
% Per-file analysis
for l = 1:nFile
    [pcaCoeff,pcaScore,pcaLatent] = pca(allCepstCoef{l});
    allPcaScore{l} = pcaScore;
end

%% visualize
l = 7;

fig(1) = figure;
subplot(311);imagesc(allZ{l}); axis xy; title('mel filtered')
subplot(312);imagesc(allCepstCoef{l}'); axis xy; title('cepstral')
subplot(313);imagesc(allPcaScore{l}'); axis xy; title('pca score')

color = 'rgbymkc';
fig(2) = figure; hold on; view(3); grid on
xlabel('x');ylabel('y');zlabel('z')

for k = 1:7
    figure(fig(1));
    pause(1);
    [x,y]= ginput(2);
    x = fix(x); 
    figure(fig(2));
    plot3(allPcaScore{l}(x(1):x(2),1),allPcaScore{l}(x(1):x(2),2),allPcaScore{l}(x(1):x(2),3),['x' color(k)])
end
[x,y,z] = sphere;
surf(x,y,z)