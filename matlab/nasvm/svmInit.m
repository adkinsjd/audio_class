function svmModel = svmInit(svmModel,X,Y)
% svmModel = svmInit(svmModel,X,Y)
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
nDat = size(X,1);

% normalize input
%mu = mean(X,1);
%sigma = std(X,0,1);
%X = (X - repmat(mu,nDat,1))./repmat(sigma,nDat,1);

%svmModel.mu = mu;
%svmModel.sigma = sigma;
svmModel.X = X;
svmModel.Y = Y;
svmModel.C = 1;

% precompute the kernel matrix
K = zeros(nDat,nDat);
for k = 1:nDat
    for l = 1:nDat
        K(k,l) = svmKernel(svmModel,svmModel.X(k,:),svmModel.X(l,:));
    end
end

svmModel.K = K;
svmModel.alpha = zeros(nDat,1);
svmModel.bias = 0;
