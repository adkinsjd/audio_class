% test various algorithm for NASVM
% 
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;

%% generate synthetic data
rng(2);
nSample = 1e2;

K1 = [1 0; 0 1];
X10 = mvnrnd([0,0],K1,nSample);
X11 = mvnrnd([5,5],K1,nSample);
K2 = [30 0; 0 1];
X20 = mvnrnd([0,0],K2,nSample);
X21 = mvnrnd([5,5],K2,nSample);
K3 = [1 0; 0 30];
X30 = mvnrnd([0,0],K3,nSample);
X31 = mvnrnd([5,5],K3,nSample);

figure; hold on;
plot(X10(:,1),X10(:,2),'b+')
plot(X11(:,1),X11(:,2),'r+')
plot(X20(:,1),X20(:,2),'b+')
plot(X21(:,1),X21(:,2),'r+')
plot(X30(:,1),X30(:,2),'b+')
plot(X31(:,1),X31(:,2),'r+')

K10 = cell(size(X10,1),1);
for k = 1:size(X10,1)
    K10{k} = K1;
end
K20 = cell(size(X20,1),1);
for k = 1:size(X20,1)
    K20{k} = K2;
end
K30 = cell(size(X30,1),1);
for k = 1:size(X30,1)
    K30{k} = K3;
end
K11 = cell(size(X11,1),1);
for k = 1:size(X11,1)
    K11{k} = K1;
end
K21 = cell(size(X21,1),1);
for k = 1:size(X21,1)
    K21{k} = K2;
end
K31 = cell(size(X31,1),1);
for k = 1:size(X31,1)
    K31{k} = K3;
end

% sanity check on filter
F1 = inv(chol(K1,'lower'));
F2 = inv(chol(K2,'lower'));
F3 = inv(chol(K3,'lower'));
FX10 = X10*F1;FX20 = X20*F2;FX30 = X30*F3;
mu = repmat([5,5],nSample,1);
FX11 = (X11-mu)*F1+mu;FX21 = (X21-mu)*F2+mu;FX31 = (X31-mu)*F3+mu;
%disp(cov(FX20)); disp(cov(FX21));
%disp(cov(X20)); disp(cov(X21));

figure;
subplot(211); hold on;
plot(X10(:,1),X10(:,2),'b+'); plot(X11(:,1),X11(:,2),'ro')
plot(X20(:,1),X20(:,2),'b+'); plot(X21(:,1),X21(:,2),'go')
plot(X30(:,1),X30(:,2),'b+'); plot(X31(:,1),X31(:,2),'mo')
subplot(212); hold on; 
plot(FX10(:,1),FX10(:,2),'b+'); plot(FX11(:,1),FX11(:,2),'ro')
plot(FX20(:,1),FX20(:,2),'b+'); plot(FX21(:,1),FX21(:,2),'go')
plot(FX30(:,1),FX30(:,2),'b+'); plot(FX31(:,1),FX31(:,2),'mo')

%% test quad NASVM 
% ======= on unimodal dataset
X = [X10;X11];
Y = [-ones(size(X10,1),1);ones(size(X11,1),1)];
K = [K10;K11];

% standard
svmModel = fitcsvm(X,Y,'Standardize',false,'KernelFunction','linear');
[~,score] = predict(svmModel,X);

% NA
svmModel2.kernelFcn = 'linear';
svmModel2 = quadTrainNASVM(svmModel2,X,Y);
score2 = zeros(size(X,1),1);
for k =1:size(X,1)
    score2(k) = svmOutput(svmModel2,X(k,:));
end

figure; hold on;
plot(score(:,2));plot(score2);
legend('fitcsvm','quadTrain')

% visualize the dataset
w = svmWeight(svmModel2);
b = svmModel2.bias;
%nX10 = (X10 - repmat(svmModel2.mu,size(X10,1),1))./repmat(svmModel2.sigma,size(X10,1),1);
%nX11 = (X11 - repmat(svmModel2.mu,size(X11,1),1))./repmat(svmModel2.sigma,size(X11,1),1);
%nX = [nX10;nX11];

figure; hold on;
plot(X(Y==-1,1),X(Y==-1,2),'b+')
plot(X(Y==1,1),X(Y==1,2),'r+')
wX1 = [min(X(:,1)),max(X(:,1))];
wX2 = (-b-w(1)*wX1)/w(2);
plot(wX1,wX2);

% ======= on mixed dataset
X = [X10;X11;X20;X21;X30;X31];
Y = [-ones(size(X10,1),1);ones(size(X11,1),1);-ones(size(X20,1),1);ones(size(X21,1),1);-ones(size(X30,1),1);ones(size(X31,1),1)];
K = [K10;K11;K20;K21;K30;K31];

% standard
svmModel = fitcsvm(X,Y,'Standardize',false,'KernelFunction','linear');
[~,score] = predict(svmModel,X);

% NA
svmModel2.kernelFcn = 'linearNA';
F = cell(numel(K),1);
for k = 1:numel(K)
    F{k} = inv(chol(K{k},'lower'));
end
svmModel2.F = F;
svmModel2 = quadTrainNASVM(svmModel2,X,Y);
score2 = zeros(size(X,1),1);
for k = 1:size(X,1)
    score2(k) = svmOutput(svmModel2,X(k,:));
end
figure; hold on;
plot(score(:,2));plot(score2);
legend('fitcsvm','quadTrain')

% visualize the dataset
w = svmWeight(svmModel2);
b = svmModel2.bias;

FX = zeros(size(X));
for k = 1:size(X,1)
    if svmModel2.Y(k) == -1
        FX(k,:) = X(k,:)*svmModel2.F{k};
    else
        FX(k,:) = (X(k,:)-[5,5])*svmModel2.F{k}+[5,5];
    end
end

figure; hold on;
plot(FX(Y==-1,1),FX(Y==-1,2),'b+')
plot(FX(Y==1,1),FX(Y==1,2),'r+')
wX1 = [min(FX(:,1)),max(FX(:,1))];
wX2 = (-b-w(1)*wX1)/w(2);
plot(wX1,wX2);

%% test incremental NASVM
[svmModel,nw] = incTrainNASVM(X,Y,K,false);
%[svmModel,nw] = incTrainNASVM(X,y,[],false);

for k = 1:size(svmModel.sv,1)
    if svmModel.svl(k) == -1
        if svmModel.alpha(k) == 1
            plot(svmModel.sv(k,1),svmModel.sv(k,2),'bs','linewidth',10)
        else
            plot(svmModel.sv(k,1),svmModel.sv(k,2),'bo','linewidth',10)
        end
    elseif svmModel.svl(k) == 1
        if svmModel.alpha(k) == 1
            plot(svmModel.sv(k,1),svmModel.sv(k,2),'rs','linewidth',10)
        else
            plot(svmModel.sv(k,1),svmModel.sv(k,2),'ro','linewidth',10)
        end
    end
end

w = svmWeight(svmModel);
b = svmModel.bias;
wX1 = [min(X(:,1)),max(X(:,1))];
wX2 = (-b-w(1)*wX1)/w(2);
plot(wX1,wX2);

figure; plot(nw); title('SVM weight vector norm')

%% test smo NASVM