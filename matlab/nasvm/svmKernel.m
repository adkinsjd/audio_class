function out = svmKernel(svmModel,X1,X2)
    if strcmp(svmModel.kernelFcn,'linear')
        out = X1*X2'; 
    elseif strcmp(svmModel.kernelFcn,'linearNA')
        [~,idx1] = min(sum(abs( repmat(X1,size(svmModel.X,1),1)-svmModel.X ),2));
        [~,idx2] = min(sum(abs( repmat(X2,size(svmModel.X,1),1)-svmModel.X ),2));
        if svmModel.Y(idx1)==-1
            FX1 = X1*svmModel.F{idx1};
        else
            FX1 = (X1-[5,5])*svmModel.F{idx1}+[5,5];
        end
        if svmModel.Y(idx2)==-1
            FX2 = X2*svmModel.F{idx2};
        else
            FX2 = (X2-[5,5])*svmModel.F{idx2}+[5,5];
        end
        out = FX1*FX2';
    elseif strcmp(svmModel.kernelFcn,'gaussian')
        out = exp( -norm(X1-X2,2)^2 / svmModel.kernelScale^2 );
    else
        error('unsupported kernel type');
    end
end