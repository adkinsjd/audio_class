function score = svmOutput(svmModel,X)
    score = 0;
    for k = 1:numel(svmModel.Y)
        score = score + svmModel.alpha(k)*svmModel.Y(k)*svmKernel(svmModel,svmModel.X(k,:),X);
    end;
    score = score + svmModel.bias;
end
