function w = svmWeight(svmModel)
% Compute SVM weights
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
    w = zeros(size(svmModel.X,2),1);
    for k = 1:numel(svmModel.alpha)
        if strcmp(svmModel.kernelFcn, 'linear')
            w = w + svmModel.alpha(k)*svmModel.Y(k)*(svmModel.X(k,:))';
        elseif strcmp(svmModel.kernelFcn, 'linearNA')
            if svmModel.Y(k) == -1
                w = w + svmModel.alpha(k)*svmModel.Y(k)*(svmModel.X(k,:)*svmModel.F{k})';
            else
                w = w + svmModel.alpha(k)*svmModel.Y(k)*((svmModel.X(k,:)-[5,5])*svmModel.F{k}+[5,5])';
            end
        else
            error('unsupported kernel type');
        end
    end
end