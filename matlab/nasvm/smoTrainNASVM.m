function svmModel = smoTrainNASVM(X,Y,R)
% svmModel = smoTrainNASVM(X,Y,R)
%
% Long Le
% University of Illinois
%

% normalize input
nDat = size(X,1);
X = (X - repmat(mean(X,1),nDat,1))./repmat(std(X,0,1),nDat,1);

svmModel.X = X;
svmModel.kernelScale = 1;
svmModel.Y = Y;
svmModel.R = R;
svmModel.alpha = zeros(nDat,1);
svmModel.thresh = zeros(nDat);
svmModel.C = 1;
svmModel.tol = 1e-3;

% precompute the kernel matrix
K = zeros(nDat,nDat);
for k = 1:nDat
    for l = 1:nDat
        K(k,l) = svmKernel(svmModel,k,l);
    end
end

svmModel.K = K;

% main
nChange = 0;
examAll = 1;
while nChange > 0 || examAll
    nChange = 0;
    if examAll
        for k = 1:nDat
            nChange = nChange+examineExample(svmModel,k);
        end
    else
        for k = 1:nDat
            if svmModel.alpha(k) > 0 && svmModel.alpha(k) < C
                nChange = nChange+examineExample(svmModel,k);
            end
        end
    end
    
    if examALl == 1
        examAll = 0;
    elseif nChange == 0
        examAll = 1;
    end
end

end

function ret = examineExample(svmModel,idx2)
    y2=svmModel.Y(idx2);
    alph2 = svmModel.alpha(idx2);
    E2 = svmOutput(svmModel,idx2) - y2;
    r2 = E2*y2;
    if (r2 < -tol && alph2 < svmModel.C) || (r2 > tol && alph2 > 0)
        if sum(svmModel.alpha > 0 & svmModel.alpha < svmModel.C) > 1
            idx1 = 0; % TODO: second choice heuristic
            if takeStep(idx1,idx2)
                ret = 1;
                return;
            end
        end
        for k = 1:nDat
            if svmModel.alpha(k) > 0 && svmModel.alpha(k) < svmModel.C
                idx1 = k;
                if takeStep(idx1,idx2)
                    ret = 1;
                    return;
                end
            end
        end
    end
    ret = 0;
end
