function [newState,specDet,speclog_prob_noise_ridge_cum] = detectOneFile(state,varargin)
% Detect acoustic events in one file
%
% Long Le 
% University of Illinois
% longle1@illinois.edu
%

% compute spectrogram
if numel(varargin) >= 2
    if isa(varargin{1},'numeric')
        y = varargin{1};
        fs = varargin{2}; % TODO: fixed hardcoded freq
    else
        filename = varargin{1};
        fs = varargin{2};
        [y,fsFile] = audioread(filename);
        y = resample(y(:,1), fs, fsFile)*10;
    end
else
    error('minimum arg required');
end

if numel(varargin) >= 4
    nBlk = varargin{3};
    nInc = varargin{4};
else
    nBlk = 256;
    nInc = nBlk/2;
end

if numel(varargin) >= 5
    isPlot = varargin{5};
else
    isPlot = false;
end

[S,tt,ff] = mSpectrogram(y,fs,nBlk,nInc);
nFc = nBlk/2;

linkOffset = 2;
btLen = 0.016;
indicatorCountdown = ceil(btLen*fs/nInc);
alpha_ridge = exp(-nInc/(btLen*fs));
supThresh = 3/(1-alpha_ridge); % suppression threshold
backtracklen = ceil(btLen*fs/nInc);

if isempty(state)
    noiseFloor = 0.01*ones(nFc,1);
    indicatorLast = zeros(nFc,1);
    log_prob_noise_ridge_cum = zeros(nFc,1);
    freAcc = 1:nFc;
    specridge_link_back = zeros(nFc,backtracklen-1); % # links = backtracklen - 1
else
    noiseFloor = state.noiseFloor;
    indicatorLast = state.indicatorLast;
    log_prob_noise_ridge_cum = state.log_prob_noise_ridge_cum;
    freAcc = state.freAcc;
    specridge_link_back = state.specridge_link_back;
end

%S = padarray(S,[0,backtracklen],0,'post'); % pad for finalization
speclog_prob_noise_ridge_cum = zeros(nFc, size(S,2));
specDet = zeros(nFc, size(S,2));
%speclog_prob_noise_ridge = zeros(nFc, size(S,2));
for k = 1:size(S,2)
    s = S(:,k);
    
    %ridge_link_back = zeros(nFc,1);
    %log_prob_noise_ridge = zeros(nFc,1);
    log_prob_noise_ridge_cum_last = log_prob_noise_ridge_cum;
    for l=1:nFc
        % noise tracking
        if(s(l) > noiseFloor(l))
            indicatorLast(l) = indicatorLast(l)-1;
            if(indicatorLast(l) <= 0) % noise period
                noiseFloor(l) = noiseFloor(freAcc(l))*1.01;
            else % signal period
                noiseFloor(l) = noiseFloor(freAcc(l))*1.005;
            end
        else % noise period
            noiseFloor(l) = max(1e-6,noiseFloor(freAcc(l))*0.99);
            indicatorLast(l) = indicatorCountdown;
        end
        
        % Bellman equation
        log_prob_noise_ridge = max(0,log(s(l)^2/noiseFloor(l)^2));
        lLow = max(1,l-linkOffset);
        lHigh = min(nFc,l+linkOffset);
        winFun = 1-5e-2/linkOffset*abs((lLow:lHigh)'-l);
        objFun = alpha_ridge*log_prob_noise_ridge_cum_last(lLow:lHigh)+...
            log_prob_noise_ridge*winFun;
        [log_prob_noise_ridge_cum(l),freAcc(l)] = max(objFun);
        freAcc(l) = lLow + freAcc(l)-1;
        %ridge_link_back(l) = 1 + min(nFc-1,max(0,l-1 - linkOffset + idx-1));
        %backBinLinks{ridge_link_back(l)} = [backBinLinks{ridge_link_back(l)} l];
    end
    %specridge_link_back = [specridge_link_back(:,2:end) ridge_link_back];
    
    % max pooling log_prob
    log_prob_noise_ridge_cum_last = log_prob_noise_ridge_cum; 
    for l=1:nFc
        % find the local max
        lLow = max(1,l-linkOffset);
        lHigh = min(nFc,l+linkOffset);
        currBand = (lLow:lHigh);
        [val,idx] = max(log_prob_noise_ridge_cum_last(currBand));
        if l == currBand(idx)
            log_prob_noise_ridge_cum(l) = val;
        else
            log_prob_noise_ridge_cum(l) = 0;
        end
    end
    %log_prob_noise_ridge_cum = log_prob_noise_ridge_cum_loMax;
    %aThresh = sort(log_prob_noise_ridge_cum_now,'ascend'); % adaptive threshold
    
    % per-bin suppressed stats
    detected_ridge_back = zeros(nFc,1);
    for l=1:nFc
        % abs threshold explanation: see the Bellman equation
        %if log_prob_noise_ridge_cum_now(l) > max(aThresh(end-5+1),8/(1-alpha_ridge))
        if log_prob_noise_ridge_cum(l) > supThresh
            %{
            currBin = l;
            for m = size(specridge_link_back,2):-1:1
                if (currBin > 0)
                    currBin = specridge_link_back(currBin,m);
                else
                    break;
                end
            end
            %}

            %if backTrackLink(l) > 0 && log_prob_noise_ridge_cum_now(l) < log_prob_noise_ridge_cum_now(backTrackLink(l))
            %    log_prob_noise_ridge_cum_now(backTrackLink(l)) = 0; % reset trivial value functions
            %end
            detected_ridge_back(l) = log_prob_noise_ridge_cum(l)-supThresh;
        end
    end
    
    % returning
    if (k > round(backtracklen/2))
        speclog_prob_noise_ridge_cum(:,k-round(backtracklen/2)) = log_prob_noise_ridge_cum;    
        specDet(:,k-round(backtracklen/2)) = detected_ridge_back;
        %speclog_prob_noise_ridge(:,k-backtracklen) = log_prob_noise_ridge;
    end
    
    %{
    for l = 1:nFc
        if numel(backBinLinks{l}) > 1
            [~,sIdx] = sort((1 - 0.5/2*abs(backBinLinks{l}'-l)).*log_prob_noise_ridge(backBinLinks{l}),'ascend');
            log_prob_noise_ridge_cum_now(backBinLinks{l}(sIdx(2:end))) = 0; % reset cum_log_prob
        end
    end
    
    for l = 1:nFc
        currBands = 1+max(0,l-1 - 2):1+min(nFc-1,l-1 + 2);
        [~, sIdx] = sort(log_prob_noise_ridge_cum_now(currBands),'ascend');
        if currBands(sIdx(1)) == l
            log_prob_noise_ridge_cum_now(currBands(sIdx(2:end))) = 0;
        end
    end
    %}
end

newState.noiseFloor = noiseFloor;
newState.indicatorLast = indicatorLast;
newState.log_prob_noise_ridge_cum = log_prob_noise_ridge_cum;
newState.freAcc = freAcc;
newState.specridge_link_back = specridge_link_back;
    
if isPlot == 1
    figure;
    ax(1) = subplot(311); imagesc(tt,ff,log(S)); axis xy; title('S')
    ax(2) = subplot(312); imagesc(tt,ff,speclog_prob_noise_ridge_cum); axis xy; title('speclog\_prob\_noise\_ridge')
    ax(3) = subplot(313); imagesc(tt,ff,specDet); axis xy; title('specDet')
    linkaxes(ax,'xy')
elseif isPlot == 2
    figure;
    ax(1) = subplot(311); imagesc(S); axis xy; title('S')
    ax(2) = subplot(312); imagesc(speclog_prob_noise_ridge_cum); axis xy; title('speclog\_prob\_noise\_ridge')
    ax(3) = subplot(313); imagesc(specDet); axis xy; title('specDet')
    linkaxes(ax,'xy')
end
