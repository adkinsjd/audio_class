function prob = GCWA_TFRSVM(dataList,servAddr,DB,PWD)

DATA = 'data';
EVENT = 'event';
fPath = pwd;%fileparts(mfilename('fullpath'));

% load the model
%load([fPath '/GCWA_TFRSVM.mat'], 'SVMModel');
load([fPath '/GCWA_TFRSVM.mat'], 'mSVMModel');

% read the data list
prob = zeros(0,1);
fid = fopen(dataList,'r+');
if fid > 0
    k = 0;
    while 1
        fname = fgetl(fid);
        if fname  == -1
            break;
        end
        k = k+1;
        prob(k) = 0;

        events = IllColGet(servAddr,DB, 'nan', PWD, EVENT, fname);
        if ~iscell(events)
            continue;
        else
            aEvent = events{1};
            if isfield(aEvent,'TFRidgeFeat')
                %[~,score] = predict(SVMModel,aEvent.TFRidgeFeat);
                %prob(k) = score(2);
                prob(k) = classifier(mSVMModel,aEvent.TFRidgeFeat);
            elseif isfield(aEvent,'feat')
                %[~,score] = predict(SVMModel,aEvent.feat);
                %prob(k) = score(2);
                prob(k) = classifier(mSVMModel,aEvent.feat);
            else
                continue;
            end
        end
    end
    fclose(fid);
end

%fprintf(1,'[');
if numel(prob) > 0
    str = sprintf('%f,',prob);
    fprintf(1,'%s',str(1:end-1));
end
%fprintf(1,']\n');

% clean the dataList after read
fid = fopen(dataList,'w');
fclose(fid);

end

function score = classifier(mSVMModel, feat)
    if strcmp(mSVMModel.kernelFcn,'gaussian')
        nFeat = (feat - mSVMModel.mu)./mSVMModel.sigma;
        score = 0;
        for k = 1:numel(mSVMModel.svl)
            score = score + mSVMModel.alpha(k)*mSVMModel.svl(k)*exp( -norm(mSVMModel.sv(k,:)-nFeat,2)^2 / mSVMModel.kernelScale^2 );
        end
        score = score + mSVMModel.bias;
        
        % Platt scaling
        func = eval(mSVMModel.ScoreTransform);
        score = func(score);
    else
        error('unsupported kernel type');
    end
end

% these subroutines must be included in this files to force a compilation
% otherwise the dependency analysis will skip them.
function y = sigmoid(x,a,b)
    y = 1/(1+exp(a*x+b));
end

function y = step(x,lb,ub,pi)
    if x < lb
        y = 0;
    elseif x >= lb && x <= ub
        y = pi;
    elseif x > ub
        y = 1;
    end
end

