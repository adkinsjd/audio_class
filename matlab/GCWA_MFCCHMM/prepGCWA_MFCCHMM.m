% Train a GCWA classifier using MFCC feature and HMM
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;

%rootDir = 'C:/Users/Long/Projects/';
%rootDir = 'C:/cygwin64/home/BLISSBOX/';
rootDir = 'C:/cygwin64/home/UyenBui/';
addpath([rootDir 'voicebox/'])
addpath([rootDir 'cohmm/'])
addpath([rootDir 'node-paper/'])
addpath([rootDir 'jsonlab/'])
addpath([rootDir 'V1_1_urlread2'])
addpath([rootDir 'sas-clientLib/matlab/src'])

%servAddr = '128.32.33.227';
servAddr = 'acoustic.ifp.illinois.edu:8080';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
DATA = 'data';
EVENT = 'event';

%% manual label data
%q.t1 = datenum(2015,12,01,00,00,00); q.t2 = datenum(2016,01,20,00,00,00);
%q.t1 = datenum(2016,03,07,15,00,00); q.t2 = datenum(2016,03,08,00,00,00);
q.t1 = datenum(2016,03,09,18,41,00); q.t2 = datenum(2016,03,09,19,01,00);
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);
interactiveTag(events, 43, servAddr, DB, USER, PWD, DATA, EVENT);

%% manual segment GCWA data
q.tag = 'GCWA';
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);
%files = dir('../network-paper/genCascade/data/GCW/');
%files = dir('data/GCWA/');

nS = 6;
blockSize=512;

for k = 1:numel(events)
    fprintf(1,'%s at %d\n',events{k}.filename,k);
    %figure('units','normalized','outerposition',[0 0 1 1]);
    
    % download the binary data
    data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{k}.filename);
    try
        [y, header] = wavread_char(data);
        fs = double(header.sampleRate);
        %audiowrite(['data/GCWA/' events{l}.filename],y,double(header.sampleRate));
    catch e
        disp('missing binary data')
        %resp = IllColDelete(servAddr, DB, USER, PWD, EVENT, events{l}.filename);
        %disp(resp);
        continue;
    end
    %[y,fs] = audioread(['../network-paper/genCascade/data/GCW/' files(k).name]);
    %[y,fs] = audioread(['data/GCWA/' files(k).name]);
    %figure; plot([1:size(y,1)]/fs,abs(y));
    
    [S,tt,ff] = mSpectrogram(y,fs,blockSize);
    figure; imagesc(tt,ff,S); axis xy
    %subplot(311); imagesc(tt,ff,S); axis xy

    % manual segmentation
    [fpath,fname,fext] = fileparts(events{k}.filename);
    if exist(sprintf('localLogs/%s_seg.mat',fname),'file')
        load(sprintf('localLogs/%s_seg.mat',fname),'vals');
    else
        [xVal,yVal] = ginput(nS);
        vals = xVal;
        %{
        vals = zeros(1,nS);
        for l = 1:nS
            vals(l) = input(sprintf('Input state %d last value in seconds: ',l));
        end
        %}
        save(sprintf('localLogs/%s_seg.mat',fname),'vals');
    end
    
    %input('Press enter to continue');
    close;
end

%% inspect all data for parameters
cnt = 0;
allCepstCoef = cell(cnt,1);
allStates = cell(cnt,1);
for k = 1:numel(events)
    if isfield(events{k},'MFCCFeat')
        cepstCoef = events{k}.MFCCFeat;
    else
        data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{k}.filename);
        try
            [y, header] = wavread_char(data);
            fs = double(header.sampleRate);
        catch e
            disp('missing binary data')
            continue;
        end
        cepstCoef = melcepst(y,fs,'Mtaz',16,floor(3*log(fs)),events{k}.fftSize);
    end
        
    [fpath,fname,fext] = fileparts(events{k}.filename);
    load(sprintf('localLogs/%s_seg.mat',fname),'vals');
    states = ones(1,size(cepstCoef,1));
    firstIdx = 1;
    for l = 1:nS
        lastIdx = round(vals(l)*fs/(events{k}.fftSize/2));
        states(firstIdx:lastIdx) = l;
        firstIdx = lastIdx + 1;
    end
    
    cnt = cnt + 1;
    allCepstCoef{cnt,1} = cepstCoef;
    allStates{cnt,1} = states;
end

% PCA approximation
[pcaCoeff, pcaScore] = pca(cell2mat(allCepstCoef));
% verify pca results
tmp = cell2mat(allCepstCoef);
mAllCepstCoef = mean(tmp,1);
norm( (tmp-repmat(mAllCepstCoef,size(tmp,1),1))/(pcaCoeff') - pcaScore )
norm( tmp - (pcaScore*pcaCoeff'+repmat(mean(tmp,1),size(tmp,1),1)) )
norm( (allCepstCoef{1}-repmat(mAllCepstCoef,size(allCepstCoef{1},1),1))/(pcaCoeff') - pcaScore(1:size(allCepstCoef{1},1),:) )

allFeatMFCCrPCA = cell(numel(allCepstCoef),1); % MFCC, reduced-PCA feature
stateAllFeatMFCCrPCA = cell(nS,numel(allCepstCoef));
for k = 1:numel(allCepstCoef)
    tmp = (allCepstCoef{k} - repmat(mAllCepstCoef,size(allCepstCoef{k},1),1))/(pcaCoeff');
    allFeatMFCCrPCA{k} = tmp(:,1:3)'; % must be DxN to input to cohmmBaumWelch
    for l = 1:nS
        stateAllFeatMFCCrPCA{l,k} = allFeatMFCCrPCA{k}(:,allStates{k}==l);
    end
end
stateLen = SplitVec(sort(cell2mat(allStates')), [], 'length');

figure; hold on;
col = 'kbkgky';
for l = 1:nS
    matMFCCPCA = cell2mat(stateAllFeatMFCCrPCA(l,:));
    plot3(matMFCCPCA(1,:),matMFCCPCA(2,:),matMFCCPCA(3,:),['x' col(l)])
end
view(3)
xlabel('1');ylabel('2');zlabel('3')

p = 1-1./stateLen;
mu = zeros(3,nS);
K = zeros(3,3,nS);
for l = 1:nS
    mu(:,l) = mean(cell2mat(stateAllFeatMFCCrPCA(l,:)),2); 
    K(:,:,l) = cov(cell2mat(stateAllFeatMFCCrPCA(l,:))');
end

%% model fitting
% initialize model
cohmm.pi = [1;0;0;0;0;0];
cohmm.A = [p(1) 1-p(1) 0 0 0 0; 0 p(2) 1-p(2) 0 0 0; 0 0 p(3) 1-p(3) 0 0; 0 0 0 p(4) 1-p(4) 0; 0 0 0 0 p(5) 1-p(5); 1-p(6) 0 0 0 0 p(6)];
funStr = '@(k,feat) ';
for k = 1:nS
    funStr = [funStr sprintf('(k==%d)*mvnpdf(feat'',',k)];
    funStr = [funStr sprintf('[%.4f %.4f %.4f],',mu(:,k))];
    funStr = [funStr sprintf('[%.4f %.4f %.4f;%.4f %.4f %.4f;%.4f %.4f %.4f])+',K(:,:,k)')];
end
funStr(end) = ';';
eval(['cohmm.B = ' funStr]);

% optimize the model
newCohmm = cohmmBaumWelch(cohmm,allFeatMFCCrPCA);
save('localLogs/GCWA_MFCCHMM.mat','newCohmm');

%% Verify trained model
nLogProb = zeros(1,numel(allFeatMFCCrPCA));
for k = 1:numel(allFeatMFCCrPCA)
    [estStates,logProb] = cohmmViterbi(newCohmm,allFeatMFCCrPCA{k});
    nLogProb(k) = logProb/size(estStates,2);
    
    figure; hold on;
    plot(allStates{k},'b'); plot(estStates,'r');
    legend('true states','estimated states');
    title(sprintf('k = %d, normalized logProb is %.4f',k,nLogProb(k)))
end

%% try all input
if isfield(q,'tag')
    q = rmfield(q,'tag');
end
events = IllQuery(servAddr, DB, USER, PWD, EVENT, q);

nLogProb = zeros(1,numel(events));
parfor k = 1:numel(events)
    if isfield(events{k},'MFCCFeat')
        cepstCoef = events{k}.MFCCFeat;
    else
        data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{k}.filename);
        try
            [y, header] = wavread_char(data);
            fs = double(header.sampleRate);
        catch e
            error('missing binary data')
        end
        cepstCoef = melcepst(y,fs,'Mtaz',16,floor(3*log(fs)),events{1}.fftSize);
    end
    tmp = (cepstCoef - repmat(mAllCepstCoef,size(cepstCoef,1),1))/(pcaCoeff');
    featMFCCrPCA = tmp(:,1:3)'; % DxN

    [estStates,logProb] = cohmmViterbi(newCohmm,featMFCCrPCA);
    nLogProb(k) = logProb/size(estStates,2);
    
    %{
    figure;
    plot(estStates);
    title(sprintf('k = %d, normalized logProb is %.4f',k,nLogProb(k)))
    %}
end

% find all instances of GCWA
isGCWA = false(1,numel(events));
parfor k = 1:numel(events)
    if isfield(events{k},'tag') && strcmp(events{k}.tag,'GCWA')
        isGCWA(k) = true;
    end
end

figure;
subplot(211); stem(nLogProb);
subplot(212); stem(isGCWA);

%% test GCWA_MFCCHMM

% generate a sample dataList file
load('~/Downloads/events_20160122.mat');
fid = fopen('dataList','w+');
for k =1:9
    fprintf(fid,'%s\n',events{k}.filename);
end
fprintf(fid,'%s',events{10}.filename);
fclose(fid);

% create Platt-scaled models
load('~/Downloads/newCohmm4.mat','newCohmm');
save('GCWA_MFCCHMM.mat','newCohmm');

% test the m function
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
PWD = 'publicPwd';
prob = GCWA_MFCCHMM(servAddr,DB,PWD)

% test the binary function
%mcc -v -R -nodisplay -m GCWA_TFRSVM.m
sprintf('./run_GCWA_MFCCHMM.sh /usr/local/MATLAB/MATLAB_Runtime/v85/ %s %s %s',servAddr,DB,PWD)
