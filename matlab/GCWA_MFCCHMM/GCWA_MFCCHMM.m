function prob = GCWA_MFCCHMM(dataList,servAddr,DB,PWD)

DATA = 'data';
EVENT = 'event';
fPath = pwd;%fileparts(mfilename('fullpath'));

% load the model
load([fPath '/GCWA_MFCCHMM.mat'], 'newCohmm');
mvnpdf(1,0,1); % dummy function call to ensure it is compiled

% read the data list
prob = zeros(0,1);
fid = fopen(dataList,'r+');
if fid > 0
    k = 0;
    while 1
        fname = fgetl(fid);
        if fname  == -1
            break;
        end
        k = k+1;
        prob(k) = 0;

        data = IllGridGet(servAddr, DB, 'nan', PWD, DATA, fname);
        try
            [y, header] = wavread_char(data);
        catch e
            continue;
        end
        fs = double(header.sampleRate);
        frameSize = 2^floor(log2(0.03*fs));
        featMFCC = melcepst(y,fs,'Mtaz',3,floor(3*log(fs)),frameSize)';
        logProb = cohmmForwBack(newCohmm,featMFCC);
        prob(k) = exp(logProb/size(featMFCC,2));
    end
    fclose(fid);
end

%fprintf(1,'[');
if numel(prob) > 0
    str = sprintf('%f,',prob);
    fprintf(1,'%s',str(1:end-1));
end
%fprintf(1,']\n');

% clean the dataList after read
fid = fopen(dataList,'w');
fclose(fid);

end

