function Y = manLabel(S,F,T)
% Manually label data
%
% Long Le
% University of Illinois
%

figure('units','normalized','outerposition',[0 0 1 1]);
imagesc(T,F,log(abs(S)))
axis xy
xlabel('Time (s)');
ylabel('Frequency (Hz)')
fprintf(1,'Press the return key to terminate labeling\n');
Y = zeros(0,2);
cnt = 0;
while true
    [x1,~] = ginput(1);
    if isempty(x1)
        break
    end
    [x2,~] = ginput(1);
    if isempty(x2)
        break
    end
    
    fprintf(1,['A label added ' num2str(x1) ' ' num2str(x2) '\n']);
    cnt = cnt + 1;
    Y(cnt,:) = [x1,x2];
end