#!/bin/bash

for file in *.png; do
    echo convert "$file" $(echo "$file" | sed 's/\.jpg$/\.eps/')
done
