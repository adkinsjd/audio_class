# Overview #

Library for interference-robust audio classification with limited training data

Application: Golden-Cheeked Warblers' Type-A song detection

### What is this repository for? ###

* Libraries in C, Java, Matlab, Python of an interference-robust audio classification algorithm  with limited training data
* [Demo on Java/Android](https://play.google.com/store/apps/details?id=com.longle1.spectrogram)
* [Android source code](https://bitbucket.org/longle1/sas-sensor)

### How do I get set up? ###

* Summary of set up: TBU
* Dependencies: TBU

### Who do I talk to? ###

* Long N. Le <longle2718@gmail.com>