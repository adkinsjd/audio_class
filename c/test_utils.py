'''
Test the C computation in Python 

Long Le <longle1@illinois.edu>
University of Illinois
'''

import numpy as np
import sys,os
sys.path.append(os.path.expanduser('~')+'/gcwa/python/')
sys.path.append('../python/')
import utils
from scipy.io import wavfile

FIXED_POINT=16

def winGen(nBlk):
    win = np.hanning(nBlk)
    fid = sys.stdout
    for k in range(len(win)):
        #fid.write(format(int(win[k]*2**(FIXED_POINT-1)),'#010x')+',')
        fid.write(format(int(win[k]*2**(FIXED_POINT-1)),'#06x')+',')

def readCMat(filename):
    # Read C results
    mat = []
    with open(filename,'r') as fid:
        for line in fid:
            tokens = line.split(',')
            #print('tokens = %s' % tokens)
            mat.append([float(tokens[k]) for k in range(len(tokens))])
            #print('mat[-1] = %s' % mat[-1])
    #mat = np.array(mat).T
    #print('mat = %s' % mat)
    return mat

def pySim(filename,nBlk,nInc,nFft):
    # Python computation
    fs,y = wavfile.read(filename) 
    y = y/2**(FIXED_POINT-1) # rescale data
    # enframes (temporal)
    Y = []
    i0 = 0 # an index into the y buffer
    # circular buffer
    buf = np.zeros(nBlk)
    bufIdx = 0
    while True:
        i1 = min(len(y),i0+nInc)
        #print('i0 = %s, i1 = %s' % (i0,i1))
        if i0 >= i1:
            break

        #print('bufIdx+i1-i0 = %s' % (bufIdx+i1-i0))
        buf[bufIdx:bufIdx+i1-i0] = y[i0:i1]
        bufIdx = (bufIdx+i1-i0) % nBlk
        Y.append(np.array(buf))

        i0 = min(len(y),i0+nInc)
    Y = np.array(Y).T

    # compute spectrogram
    F,T,S = utils.spectrogram(y,fs,nBlk,nInc,nFft)

    return Y,S

