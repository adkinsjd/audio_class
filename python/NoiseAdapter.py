'''
Noise adapter

Long Le <longle1@illinois.edu>
University of Illinois
'''

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal


class NoiseAdapter:

    def __init__(self, nCh, minVal=1e-3, sigDur=5, iniVal=1e-1):
        '''
        input:
                nCh - the number of channels
                minVal - minimal noise floor
                sigDur - average duration of a continuous signal component in samples
                iniVal - initial noise floor
        '''
        self.nCh = nCh
        self.iniVal = iniVal
        self.minVal = minVal  # np.finfo(float).eps
        self.indicator_countdown = sigDur
        alpha = 0.02
        slow_scale = 0.5
        self.floor_up = 1 + alpha
        self.floor_up_slow = 1 + alpha * slow_scale
        self.floor_down = 1 - alpha

        self.noisefloor = np.zeros(nCh)
        self.indicator_last = np.zeros(nCh)
        for i in range(nCh):
            self.noisefloor[i] = iniVal
            self.indicator_last[i] = self.indicator_countdown

        return

    def update(self, frame, i):
        if frame[i] > self.noisefloor[i]:
            self.indicator_last[i] -= 1
            if self.indicator_last[i] < 0:  # noise period
                self.noisefloor[i] = max(
                    self.minVal, self.noisefloor[i] * self.floor_up)
            else:  # signal period
                self.noisefloor[i] = max(self.minVal, self.noisefloor[
                                         i] * self.floor_up_slow)
        else:  # noise period
            self.noisefloor[i] = max(self.minVal, self.noisefloor[
                                     i] * self.floor_down)
            self.indicator_last[i] = self.indicator_countdown

        return

    def updateAllCh(self, frame):
        for i in range(self.nCh):
            self.update(frame, i)

        return self.noisefloor

    def track(self, tseq):
        noisebg = np.zeros(tseq.shape)
        for ti in range(tseq.shape[0]):
            noisebg[ti, :] = self.updateAllCh(tseq[ti, :])

        return noisebg

if __name__ == '__main__':
    print(__doc__)
