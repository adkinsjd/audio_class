'''
The main file for processing gcwa

Long Le <longle1@illinois.edu>
University of Illinois
'''

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.fftpack import fft
import audio_class
from sklearn.decomposition import MiniBatchDictionaryLearning,FastICA
from obspy.signal.util import enframe
import os
import sys
sys.path.insert(0,sys.path[0]+'/../../sas-clientLib/python/src/')
#print(sys.path)
from sasclient import *
from datetime import datetime
import pickle
import simpleaudio as sa
from python_speech_features import mfcc

dataRoot = '../data/'
files = os.listdir(dataRoot)
nBlk = 512
nInc = 256

##################################
def downDataLocal():
	# Get data from Illiad and store locally

	servAddr = 'acoustic.ifp.illinois.edu:8080'
	DB = 'publicDb'
	USER = 'nan'
	PWD = 'publicPwd'
	DATA = 'data'
	EVENT = 'event'
	dataRoot = '../data/'

	q = {'t1':datetime(2016,3,20,00,00,00),\
		 't2':datetime(2016,3,30,00,00,00),\
		 'loc': [30.828175,-98.5795],\
		 'rad': 200}
	events = IllQuery(servAddr,DB, USER, PWD, EVENT, q);
	print("Number of events found is "+str(len(events)))
	# bytes
	for idx,event in enumerate(events):
		print(idx)
		data = IllGridGet(servAddr, DB, USER, PWD, DATA, event['filename'])
		if 'RIFF' == data[0:4].decode('utf-8'):
			with open(dataRoot+event['filename'], 'wb') as fid:
				fid.write(data)
	return

##################################
def singleDataView():
	# Inspect a single data file with
	# online dictionary learning and sparse coding of GCWA songs

	fs,data = audio_class.readSegment(dataRoot+'GCWA_col1_ele1.wav')
	data = signal.resample(data,int(len(data)/fs*16e3))
	fs = int(16e3)
	T = np.arange(len(data))/fs
	f,t,S = signal.spectrogram(data,fs,window='hann',nperseg=nBlk,noverlap=nBlk-nInc)

	fig = plt.figure();
	plt.pcolormesh(t,f,np.log(S))
	plt.ylabel('Frequency (Hz)')
	plt.xlabel('Time (s)')
	plt.title('Sampling rate = '+str(fs)+' Hz')
	plt.autoscale(enable=True, axis='both', tight=True)

	X,framelen,noframes = enframe(data,win=np.hanning(nBlk),inc=nInc)
	dico = MiniBatchDictionaryLearning(n_components=5, alpha=50, n_iter=500)
	dict_ = dico.fit(X).components_

	fig = plt.figure()
	for idx,val in enumerate(dict_):
		valF = abs(fft(val))[0:nBlk//2+1]
		plt.subplot(5,2,idx*2+1)
		plt.plot(val)
		plt.subplot(5,2,idx*2+2)
		plt.plot(f,valF)
	plt.show()
	return

##################################
def labelData(dType):
	# Prepare a data set
	X = [None for x in range(len(files))] 
	Y = np.zeros((len(files),1),dtype=np.int)
	for idx,file_ in enumerate(files):
		fs,data = audio_class.readSegment(dataRoot+file_)
		data = signal.resample(data,int(len(data)/fs*16e3))
		f,t,x = signal.spectrogram(data,fs,window='hann',nperseg=nBlk,noverlap=nBlk-nInc)
		#x,framelen,noframes = enframe(data,win=np.hanning(nBlk),inc=nInc)
		mfcc_feat = mfcc(data,fs)
		print('idx '+str(idx)+' spectrogram dim '+str(np.shape(x)))
		
		# play the sound
		wave_obj = sa.WaveObject.from_wave_file(dataRoot+file_)
		play_obj = wave_obj.play()
		play_obj.wait_done()
		
		# visualize the sound
		fig = plt.figure(10)
		plt.pcolormesh(t,f,np.log(x))
		plt.ylabel('Frequency (Hz)')
		plt.xlabel('Time (s)')
		plt.autoscale(enable=True, axis='both', tight=True)
		plt.show()

		# ask for user label
		while True:
			y = input('Please input a valid label(0 or 1): ')
			try:
				y = int(y)
				if y == 0 or y == 1:
					break
			except ValueError:
				continue
		Y[idx] = y
		if dType == 0:
			X[idx] = x
		elif dType == 1:
			X[idx] = mfcc_feat
			#print(np.shape(X[idx]))
		
	# write data to a local file
	if dType == 0:
		pklFname = 'gcwa_specgram.pkl'
	elif dType == 1:
		pklFname = 'gcwa_mfcc.pkl'

	with open(pklFname, 'wb') as fid:
		pickle.dump((X,Y),fid)
	print(pklFname+' written!')
	return
##################################
def artificialData(dType):
	fs, data = audio_class.readSegment('../data_full/TypeA_Eg1.wav')
	data = data/2**15 # normalize
	#plt.plot(data)
	#plt.show()
	#print(np.shape(data))
	nData = 1000
	if dType == 0:
		scale = .01
	elif dType == 1:
		scale = .03

	mfcc_feat = mfcc(data,fs)

	X = np.zeros((nData,np.shape(mfcc_feat)[0],np.shape(mfcc_feat)[1]),dtype=np.float)
	Y = np.zeros(nData,dtype=np.int)
	datRange = range(np.shape(X)[0])
	for i in datRange[:nData//2]:
		X[i] = mfcc(np.random.normal(0.,scale,len(data))+data,fs)
		#plt.plot(X[i])
		#plt.show()
		Y[i] = 1
	for i in datRange[nData//2:]:
		X[i] = mfcc(np.random.normal(0.,scale,len(data)),fs)
		Y[i] = 0

	# write data to a local file
	if dType == 0:
		pklFname = 'gaussian.pkl'
	elif dType == 1:
		pklFname = 'gaussian_test.pkl'

	with open(pklFname, 'wb') as fid:
		pickle.dump((X,Y),fid)
	print(pklFname+' written!')
	return

if __name__ == '__main__':
	print(__doc__)
	#downDataLocal()
	#singleDataView()
	labelData(0)
	#artificialData(1)

