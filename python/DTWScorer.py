'''
DTW scorer

Long Le <longle1@illinois.edu>
University of Illinois
'''

import numpy as np
from scipy import signal

class DTWScorer:
	def __init__(self,sigDeltaF=0.1,scale = 10):
		self.sigDeltaF = sigDeltaF
		self.scale = scale
	
		return

	def computeScore(self,X1,X2):
		C = self.doubleIntCorr(X1,X2)
		V,lnk = self.maxPath(C)
		score = V[0,0]
		allPos = np.array(self.tracer(V,lnk))
		return score/len(allPos),V,lnk,allPos,C
	
	def doubleIntCorr(self,X1,X2):
		''' Compute double integral of correlation between 2 vectors
		Input
			x1,x2: input vectors
		Output
			C: correlation matrix
		'''
		radT = 5
		C = np.zeros((len(X1),len(X2)))
		for t1 in range(len(X1)):
			for t2 in range(len(X2)):
				'''
				x1 = np.nonzero(X1[t1,:])[0]
				x2 = np.nonzero(X2[t2,:])[0]
				corrVals = []
				for f1 in x1:
					for f2 in x2:
						corrVals += [np.exp(-(f1-f2)**2/self.sigDeltaF**2)]
				if len(corrVals)>0:
					C[t1,t2] = np.mean(corrVals)
				'''
				#C[t1,t2] = np.max((X1[t1,:]+X2[t2,:])/2*np.exp(-(X1[t1,:]-X2[t2,:])**2/self.sigDeltaF**2))
				x1 = np.mean(X1[max(0,t1-radT):min(len(X1),t1+radT),:],axis=0)
				x2 = np.mean(X2[max(0,t2-radT):min(len(X2),t2+radT),:],axis=0)
				#C[t1,t2] = np.mean((x1+x2)/2*np.exp(-(x1-x2)**2/self.sigDeltaF**2))
				#C[t1,t2] = 30*np.mean((x1+x2)/2)*np.dot(x1,x2)/np.linalg.norm(x1)/np.linalg.norm(x2)
				C[t1,t2] = np.dot(x1,x2)
				#C[t1,t2] = max(signal.correlate(x1,x2))

		return C

	def maxPath(self,C):
		#print(C.shape)
		V = np.empty(C.shape);
		V[:] = np.nan
		lnk = np.empty(C.shape)
		lnk[:] = np.nan
		
		pos = [0,0]
		self.dpUpdate(C,pos,V,lnk)
				
		return V,lnk

	def tracer(self,V,lnk):
		B0 = V.shape[0]
		B1 = V.shape[1]
		slope = B1/B0
		scale = self.scale
		
		allPos = []
		pos = [0,0]
		while self.isConMet(pos[0],pos[1],slope,B0,scale):
			#print(pos)
			allPos += [list(pos)] # must create a new instance
			#print('np.shape(allPos) = '+str(np.shape(allPos)))
			#print(allPos[-1])
			if lnk[pos[0],pos[1]] == 0:
				pos[0] += 1
				pos[1] += 1
			elif lnk[pos[0],pos[1]] == 1:
				pos[0] += 1
			elif lnk[pos[0],pos[1]] == 2:
				pos[1] += 1
				
		#print(allPos)
		return allPos

	def dpUpdate(self,C,pos,V,lnk):
		B0 = V.shape[0]
		B1 = V.shape[1]
		slope = B1/B0
		scale = self.scale

		if self.isConMet(pos[0],pos[1],slope,B0,scale):
			nextVals = [np.nan,np.nan,np.nan] # normal list for appending
			if self.isConMet(pos[0]+1,pos[1]+1,slope,B0,scale):
				# ensure no nan value here
				if np.isnan(V[pos[0]+1,pos[1]+1]):
					self.dpUpdate(C,pos+np.array([1,1]),V,lnk)
				nextVals[0] = V[pos[0]+1,pos[1]+1]
					
			if self.isConMet(pos[0]+1,pos[1],slope,B0,scale):
				if np.isnan(V[pos[0]+1,pos[1]]):
					self.dpUpdate(C,pos+np.array([1,0]),V,lnk)
				nextVals[1] = V[pos[0]+1,pos[1]]
				
			if self.isConMet(pos[0],pos[1]+1,slope,B0,scale):
				if np.isnan(V[pos[0],pos[1]+1]):
					self.dpUpdate(C,pos+np.array([0,1]),V,lnk)
				nextVals[2] = V[pos[0],pos[1]+1]
			
			#print(str(pos)+': '+str(nextVals))
			
			#if np.sum(np.isnan(nextVals))==5:
			if np.sum(np.isnan(nextVals))==3:
				# the final equation
				#print('nextVals = '+str(slope)+' '+str(B0)+' '+str(scale))
				#x = pos[0]+1
				#y = pos[1]+1
				#print('bool = '+str(slope/scale*x))
				#print('final pos = '+str(pos))
				#print('final val = '+str(C[pos[0],pos[1]]))
				V[pos[0],pos[1]] = C[pos[0],pos[1]]
				lnk[pos[0],pos[1]] = 0
			else:
				# recursive equation
				maxVal = np.nanmax(nextVals)
				maxIdx = np.nanargmax(nextVals)
				V[pos[0],pos[1]] = C[pos[0],pos[1]] + maxVal
				lnk[pos[0],pos[1]] = maxIdx
			
			#print('V = '+str(V[pos[0],pos[1]])+', lnk = '+str(lnk[pos[0],pos[1]]))
			
		return

	def isConMet(self,x,y,slope=1,x0=10,scale=2):
		return \
				(y <= slope*x+scale) and\
				(y >= slope*x-scale) and\
				(x >= 0) and\
				(x < x0) and\
				(y >= 0) and\
				(y < np.floor(slope*x0))
				#(y <= slope*scale*x) and\
				#(y >= slope*scale*x-slope*x0) and\
				#(y >= slope/scale*x) and\
				#(y <= slope/scale*x+slope/scale*x0) and\

