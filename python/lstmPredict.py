'''
RNN of GCWA songs

Long Le <longle1@illinois.edu>
University of Illinois
'''

import tflearn
from tflearn.data_utils import to_categorical,pad_sequences
import pickle
from sklearn.cross_validation import train_test_split
import numpy as np
import sys

#modelFile = 'gcwa_specgram'
#modelFile = 'gcwa_mfcc'
modelFile = 'gaussian'
##################################
# Sequence padding
with open(modelFile+'_test.pkl','rb') as f:
	Xu,Y = pickle.load(f)

maxlen = 250
# padding
X = np.zeros((np.shape(Xu)[0],maxlen,np.shape(Xu)[2]))
for i in range(len(Xu)):
	X[i] = pad_sequences(Xu[i].T,maxlen=maxlen,value=0.).T

##################################
# LSTM network building
if modelFile == 'gcwa_specgram':
	net = tflearn.input_data([None,maxlen,257],name='input')
elif modelFile == 'gcwa_mfcc':
	net = tflearn.input_data([None,maxlen,13],name='input')
elif modelFile == 'gaussian':
	net = tflearn.input_data([None,maxlen,13],name='input')
else:
	sys.exit('Unable to understand input, terminated!')
	
net = tflearn.lstm(net, 256, dropout=0.5, dynamic=True)
net = tflearn.fully_connected(net, 2, activation='softmax')
net = tflearn.regression(net, optimizer='adam', learning_rate=0.001,
                         loss='categorical_crossentropy')

model = tflearn.DNN(net)
model.load(modelFile+'.tfl')

prediction = model.predict(X)

cnt = 0.
for idx,val in enumerate(prediction):
	if np.argmax(val) == Y[idx]:
		cnt += 1
print('numtest = '+str(len(prediction))+', accuracy = '+str(cnt/len(prediction)))
