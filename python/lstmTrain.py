'''
RNN of GCWA songs

Long Le <longle1@illinois.edu>
University of Illinois
'''

import tflearn
from tflearn.data_utils import to_categorical,pad_sequences
import pickle
from sklearn.cross_validation import train_test_split
import numpy as np
import sys

#modelFile = 'gcwa_specgram'
#modelFile = 'gcwa_mfcc'
modelFile = 'gaussian'
##################################
# Sequence padding
with open(modelFile+'.pkl','rb') as f:
	X,Y = pickle.load(f)

maxlen = 250
trainXu,testXu,trainY,testY = train_test_split(X, Y, test_size=0.4, random_state=42)
# padding
trainX = np.zeros((np.shape(trainXu)[0],maxlen,np.shape(trainXu)[2]))
for i in range(len(trainXu)):
	trainX[i] = pad_sequences(trainXu[i].T,maxlen=maxlen,value=0.).T
testX = np.zeros((np.shape(testXu)[0],maxlen,np.shape(testXu)[2]))
for i in range(len(testXu)):
	testX[i] = pad_sequences(testXu[i].T,maxlen=maxlen,value=0.).T

trainY = to_categorical(trainY, nb_classes=2)
testY = to_categorical(testY, nb_classes=2)
##################################
# LSTM network building
if modelFile == 'gcwa_specgram':
	net = tflearn.input_data([None,maxlen,257],name='input')
elif modelFile == 'gcwa_mfcc':
	net = tflearn.input_data([None,maxlen,13],name='input')
elif modelFile == 'gaussian':
	net = tflearn.input_data([None,maxlen,13],name='input')
else:
	sys.exit('Unable to understand input, terminated!')
	
net = tflearn.lstm(net, 256, dropout=0.5, dynamic=True)
net = tflearn.fully_connected(net, 2, activation='softmax')
net = tflearn.regression(net, optimizer='adam', learning_rate=0.001,
                         loss='categorical_crossentropy')

# Training
model = tflearn.DNN(net, tensorboard_verbose=0,checkpoint_path=modelFile+'.tfl.ckpt')
model.fit(trainX, trainY, n_epoch=20,validation_set=(testX, testY), show_metric=True)
model.save(modelFile+'.tfl')

