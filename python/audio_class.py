'''
Utility functions

Long Le <longle1@illinois.edu>
University of Illinois
'''
import numpy as np
import matplotlib.pyplot as plt
import struct
from joblib import Parallel, delayed
import multiprocessing
from sklearn.metrics import roc_curve, precision_recall_curve, auc
import time
import wave

def prepareDataset():
    #========== 
    # loading the data
    tStart = time.clock()

    print('load raw audio')
    fs,dataSet = readSegment('../data/train.wav')
    #print('np.shape(dataSet) = %s' % (np.shape(dataSet),) )

    print('tElapsed = '+str(time.clock() - tStart))

    #==========
    # load the labels
    lfname = '../data/Label Track.txt'
    tMarks = []
    with open(lfname,'r') as f:
        for mLine in f:
            mTokens = mLine.split()
            #print(mTokens)
            tMarks.append([float(mTokens[0]), float(mTokens[1])])
    tMarks = np.array(tMarks)
    #print(np.shape(tMarks)

    #==========
    # compute spectrogram
    tStart = time.clock()

    tBlk = 32e-3
    tInc = 16e-3
    print('extract spectrogram')
    fSpec,_,_,tBlk,tInc = spectrographic(dataSet,fs,tBlk,tInc)
    print('rounded tBlk = %.3f' % tBlk)
    print('rounded tInc = %.3f' % tInc)

    print('tElapsed = '+str(time.clock() - tStart))

    #==========
    # compute frame labels
    tStart = time.clock()

    print('form frame labels')
    fLab = np.zeros(np.shape(fSpec)[1])
    nMarks = np.zeros(np.shape(tMarks)).astype(int)
    for k in range(len(tMarks)):
        nMarks[k,0] = int(tMarks[k,0]/tInc)
        nMarks[k,1] = int(tMarks[k,1]/tInc)
        n0 = nMarks[k,0]
        n1 = nMarks[k,1]+1
        fLab[n0:n1] = np.ones(n1-n0)

    print('tElapsed = '+str(time.clock() - tStart))

    return fs,dataSet,tMarks,nMarks,fSpec,fLab,tBlk,tInc

def readSegment(fname,t1=None,t2=None):
    with wave.open(fname,'r') as wfd:
        fs = wfd.getframerate()
        N = wfd.getnframes()
        #print('N = %s' % N)
        nCh = wfd.getnchannels()
        #print('nCh = %s' % nCh)
        nbytes = wfd.getsampwidth()
        #print('nbytes = %s' % nbytes)

        if t1 == None:
            idx1 = 0
        else:
            # round to the correct # of bytes per sample
            idx1 = max(0,min(int(t1*fs),N))//nbytes*nbytes

        if t2 == None:
            idx2 = N
        else:
            idx2 = max(0,min(int(t2*fs),N))//nbytes*nbytes
        #print('N = '+str(N))
        #print('idx1 = '+str(idx1))
        #print('idx2 = '+str(idx2))
                        
        wfd.setpos(idx1)
        buf = wfd.readframes(idx2-idx1)
        #print( 'len(buf) = %s' % len(buf) )
        
        # References:
        #   https://docs.python.org/2/library/struct.html#format-characters
        #   http://wavefilegem.com/how_wave_files_work.html
        # '<': little-endian
        # a frame contains samples from all channels at one time 
        data = np.zeros((nCh,len(buf)//(nbytes*nCh)))
        for k in range(0,len(buf),nbytes*nCh): # frame index
            for l in range(nCh):
                if nbytes == 2:
                    # signed 2 bytes
                    sample = struct.unpack('<h',buf[k+l*nbytes:k+l*nbytes+nbytes])[0]
                elif nbytes == 3:
                    # unsigned 2 bytes + signed byte
                    #print(buf[k+l*nbytes:k+l*nbytes+nbytes])
                    x,y= struct.unpack('<Hb',buf[k+l*nbytes:k+l*nbytes+nbytes])
                    sample =  x | (y << 16)
                elif nbytes == 4:
                    # signed 4 bytes
                    sample = struct.unpack('<i',buf[k+l*nbytes:k+l*nbytes+nbytes])[0]
                else:
                    raise ValueError('Unsupport sample format')

                data[l,k//(nbytes*nCh)] = sample 

    return fs,np.squeeze(data)

def spectrogram(y,fs,nBlk,nInc,nFft):
    N = len(y)
    win = np.hanning(nBlk)
    nT = int(np.ceil(N/nInc))
    #print('N = '+str(N))
    #print('nInc = '+str(nInc))
    #print('nT = '+str(nT))

    F = np.arange(nFft//2)/nFft*float(fs)
    T = np.arange(nT)*nInc/float(fs)
    S = np.empty((nFft//2,nT))
    i = 0
    for k in range(nT):
        #print('k = '+str(k))
        j = min(N,i+nBlk)

        frame = np.pad(y[i:j],(0,nBlk-(j-i)),'constant',constant_values=0)*win
        spec = np.abs(np.fft.fft(frame,nFft))
        S[:,k] = spec[0:nFft//2]

        i = min(N,i+nInc)

    #print('frame = '+str(frame))
    return F,T,S

def spectrographic(y,fs,tBlk,tInc):
    #y = signal.resample(y,int(len(y)/fs*20e3))
    #fs = 20e3
    nBlk = nFft = 2**int(np.log2(int(tBlk*fs)))
    nInc = 2**int(np.log2(int(tInc*fs)))
    #print('nBlk = %d' % nBlk)
    #print('nInc = %d' % nInc)
    tBlk = nBlk/fs
    tInc = nInc/fs

    #F,T,S = signal.spectrogram(y,fs,window='hann',nperseg=nBlk,noverlap=nBlk-nInc,nfft=nFft)
    F,T,S = spectrogram(y,fs,nBlk,nInc,nFft)
    return S,F,T,tBlk,tInc

def blks2frames(blks,bWin,bInc):
    # about blks:
    # nB: number of blocks, bLen: length of each block
    nB,bLen = np.shape(blks)
    nT = nB*bInc
    #print('nT = '+str(nT))
    #print('bInc = '+str(bInc))
    #print('bWin = '+str(bWin))

    # about blksSum: nB x 1
    blksSum = np.sum(blks,1)
    # assume bWin >= bInc
    # num of blks that can overlap on 1 side
    winLo = np.ceil(bWin/bInc).astype(int)-1
    frames = np.zeros(nT)
    for k in range(winLo,nB-winLo-1):
        #print('k = '+str(k))
        kL = max(0,k-winLo)
        kH = min(nB,k+winLo+1)
        # local max
        if blksSum[k] == np.max(blksSum[kL:kH]):
            # center blks[k,:]
            kb0 = k*bInc+(bWin-bLen)//2
            kb1 = kb0+bLen
            frames[kb0:kb1] = smooth(blks[k,:])

    return frames

def smooth(blk):
    N = len(blk)
    wN = 40
    blkSmooth = np.zeros(N)
    for k in range(N):
        blkSmooth[k] = np.mean(blk[max(0,k-wN):min(N,k+wN+1)])

    return blkSmooth#-np.min(blkSmooth)

def roc_pr_aucs(out0,out1):
    lab = np.append(np.zeros(len(out0)),np.ones(len(out1)))
    out = np.append(out0,out1)

    fpr,tpr,_ = roc_curve(lab, out)
    roc_auc = auc(fpr, tpr)
    pre,rec,_ = precision_recall_curve(lab,out)
    pr_auc = auc(rec,pre)
    return roc_auc,pr_auc

def getTF(X,fs,tInc):
    shapeX = np.shape(X)
    if len(shapeX) == 2:
        F,T = shapeX
        ff = np.arange(F)/F*fs/2
        tt = np.arange(T)*tInc
        return ff,tt
    elif len(shapeX) == 1:
        T = shapeX[0]
        tt = np.arange(T)*tInc
        return None,tt

def corrupt(y,itf,dSIR,fs,tBlk,tInc,debug=0,itfIdx=0):
    yS,_,_,tBlk,tInc = spectrographic(y,fs,tBlk,tInc)
    itfS,_,_,tBlk,tInc = spectrographic(itf,fs,tBlk,tInc)

    yP = max(yS.flatten())
    itfP = max(itfS.flatten())
    SIR = 10*np.log10(yP / itfP)
    r = 10**((SIR - dSIR) / 10)
    #print('SIR = %f' % SIR)
    #print('r = %f' % r)

    # interfered data
    yn = y + itf*r
    ynS,ff,tt,tBlk,tInc = spectrographic(yn,fs,tBlk,tInc)

    if debug >= 1:
        #plt.figure(figsize=(9, 8), dpi= 80, facecolor='w', edgecolor='k')
        plt.figure()
        '''
        plt.subplot(311)
        plt.pcolormesh(tt,ff,np.sqrt(yS))
        plt.autoscale(True,'both',True)
        plt.subplot(312)
        plt.pcolormesh(tt,ff,np.sqrt(itfS))
        plt.autoscale(True,'both',True)
        plt.subplot(313)
        '''
        plt.pcolormesh(tt,ff,np.sqrt(ynS))
        plt.autoscale(True,'both',True)
        plt.title('SIR = %.2f dB' % dSIR)
        plt.xlabel('Time (s)')
        plt.ylabel('Frequency (Hz)')
        plt.savefig('../doc/itf%d.png' % (itfIdx+1), bbox_inches='tight')
        plt.show()


    return yS,ynS,ff,tt
