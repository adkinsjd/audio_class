'''
An SAS client for detecting GCWA

Long Le <longle1@illinois.edu>
University of Illinois
'''

import numpy as np
import sys,os
sys.path.append(os.path.expanduser('~')+'/sas-client/python/src')
sys.path.append('../../sas-client/python/src')
from sasclient import *
from ridgeDTW import ridgeTracker, dtw
from wavread_char import wavread_char
import audio_class
import pickle, time
from datetime import datetime, timedelta
from scipy.signal import resample,lfilter

servAddr = 'acoustic.ifp.illinois.edu:8080'
DB = 'publicDb'
USER = 'nan'
PWD = 'publicPwd'
DATA = 'data'
EVENT = 'event'

thresh2 = .5
probEst2 = 0.2
muT2 = muP2 = np.exp(-11e-3/5.)
thresh3 = 0.25
probEst3 = 0.05
muT3 = muP3 = np.exp(-11e-3/5.)

def process(tLast=None, tNow=None):
    global thresh2,probEst2,thresh3,probEst3

    X = None
    Xp = None
    # reload template
    with open('template_ridge.pkl','rb') as fid:
        TemX_h,TemN_h = pickle.load(fid)
    # reload filters
    with open('gcwa_filters.pkl','rb') as fid:
        h1,h2 = pickle.load(fid)

    # query for possible events
    #q = {'t1':datetime(2017,2,14,0,0,0),
    #     't2':datetime(2017,2,15,0,0,0)}
    if tNow is None:
        tNow = datetime.utcnow()
    if tLast is None:
        tLast = tNow - timedelta(seconds=120)
    q = {'t1':tLast,
         't2':tNow}
    print('t1 = %s, t2 = %s' % (q['t1'],q['t2']))
    events = IllQuery(servAddr,DB,USER,PWD,EVENT,q)
    print('# of events = %d' % len(events))
    for event in events:
        # skipped processed events
        if 'GCWA_score' in event or 'GCWA_bandpass_score' in event:
            continue

        # get data
        if 'filename' not in event:
            print('no "filename" in an event: %s' % event)
            continue
        dat = IllGridGet(servAddr,DB,USER,PWD,DATA,event['filename'])
        mDict = wavread_char(dat)
        data = mDict['data']
        fs = event['fs']
        #print('np.shape(data) = %s' % np.shape(data))
        #print('fs = %s' % fs)
        # upsampling data with linear interpolation
        data = resample(data,int(len(data)/fs*24e3))
        fs = 24e3
        #print('np.shape(data) = %s' % np.shape(data))
        #print('fs = %s' % fs)

        # the main signal processing
        out1 = np.mean(np.abs(lfilter(h1,1,data)))
        out2 = np.mean(np.abs(lfilter(h2,1,data)))
        score = max(out1,out2)
        IllColPut(servAddr,DB,USER,PWD,EVENT,event['filename'], 'set', '{"GCWA_bandpass_score":%.4f}' % score);
        print('event %s (recordDate = %s): GCWA_bandpass_score = %.4f' % (event['filename'],event['recordDate'],score))
        if score > thresh2:
            #========== temporal-spectral stage
            S,_,_,tBlk,tInc = audio_class.spectrographic(data,fs,32e-3,16e-3)
            X,N = ridgeTracker(S,np.median(S),32e-3,tInc)
            Xp,Np,c = dtw([TemX_h,X],[TemN_h,N],1)
            cc = audio_class.smooth(c)
            score = np.mean(cc)
            IllColPut(servAddr,DB,USER,PWD,EVENT,event['filename'], 'set', '{"GCWA_score":%.4f}' % score);
            print('event %s (recordDate = %s): GCWA_score = %.4f' % (event['filename'],event['recordDate'],score))

            # depending on the result, 
            # update the database
            if score > thresh3:
                print('Its a GCWA!')
                IllColPut(servAddr,DB,USER,PWD,EVENT,event['filename'], 'set', '{"tag":"GCWA"}')

                probEst3 = muP3*probEst3 + (1-muP3)
            else:
                probEst3 = muP3*probEst3
            thresh3 = thresh3 + (1-muT3)*(probEst3-0.05)
            #==========

            probEst2 = muP2*probEst2 + (1-muP2)
        else:
            probEst2 = muP2*probEst2
        thresh2 = thresh2 + (1-muT2)*(probEst2-0.2)

    sys.stdout.flush()
    return X,Xp

if __name__ == '__main__':
    while True:
        process()
        time.sleep(60)
