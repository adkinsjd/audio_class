'''
Implement the prominent-region-based DTW
and experimenting with functional programming

Long Le <longle1@illinois.edu>
University of Illinois
'''

import sys,os
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import pickle
import audio_class
from joblib import Parallel,delayed

os.system("taskset -p 0xff %d" % os.getpid())

def proTFRegion(S,extF = round(0.25e3/20e3*512)):
    nF,nT = np.shape(S)
    thresh = 0.2*np.max(S,axis=0)

    R = np.zeros((nF,nT))
    for i in range(nT):
        for k in range(nF):
            if S[k,i] > thresh[i]:
                R[k,i] = 1

        proIdx = np.nonzero(nF)
        maxIdx = np.max(proIdx)
        minIdx = np.min(proIdx)

        # extending by 0.25 kHz each size
        for j in range(maxIdx, min(nF-1,maxIdx+extF)+1):
            R[j, i] = 1
        for j in range(max(0,minIdx-extF), minIdx+1):
            R[j, i] = 1

    return R

def blockProc(allS,bWin,bInc,fs):
    nF,nT = np.shape(allS)

    # params
    with open('template_region.pkl','rb') as fid:
        X0,R,w = pickle.load(fid)

    # block processing
    nB = int(np.ceil(nT/bInc)) # number of blocks (of frames)
    blks = Parallel(n_jobs=8)(delayed(blockProcPar)(k,nB,bInc,bWin,nT,allS,X0,R,w) for k in range(nB))
    blks = np.array(blks)

    out = audio_class.blks2frames(blks,bWin,bInc)

    return out[:nT]

def blockProcPar(k,nB,bInc,bWin,nT,allS,X0,R,w):
    sys.stdout.write("Progress: %d%%   \r" % (k/nB*100.) )
    frameL = k*bInc
    frameH = min(nT,k*bInc+bWin)
    X = np.pad(allS[:,frameL:frameH],((0,0),(0,bWin-(frameH-frameL))),'constant',constant_values=0)
    # warp to the dim of X
    _,c,_ = dtw(X0,R,w,X,0)

    return c

def dtw(X0,R,w,X,axis):
    nF,nT0 = np.shape(X0)
    nF,nT = np.shape(X)

    # compute the cost matrix
    C = np.zeros((nT0,nT))
    for i in range(nT0):
        for j in range(nT):
            u = X0[:,i]*R[:,i]
            v = X[:,j]*R[:,i]
            C[i,j] = cosSim(u,v)

    # recursion
    P = np.zeros(np.shape(C))
    bt = -np.ones(np.shape(C))
    nTL = int(np.floor(0.2*nT)) # nT low
    for j in range(nTL):
        P[0,j] = C[0,j]
        bt[0,j] = 0
    for j in range(1,nT):
        values = [P[0,j-1]+w[1]*C[1,j], 0, 0, P[0,j]+w[1]*C[1,j]]
        P[1,j] = max(values)
        bt[1,j] = np.argmax(values)
    for i in range(2,nT0):
        for j in range(2,nT):
            if globalCon(P,i,j):
                # local constraints
                values = [P[i-1,j-1]+w[i]*C[i,j],
                        P[i-1,j-2]+0.5*w[i]*C[i,j-1]+0.5*w[i]*C[i,j],
                        P[i-2,j-1]+0.5*w[i-1]*C[i-1,j]+0.5*w[i]*C[i,j]]
                P[i,j] = max(values)
                bt[i,j] = np.argmax(values)

    # total similarity
    nTH = int(np.floor(0.8*nT)) # nT high
    p = np.max(P[nT0-1, nTH:nT])
    offIdx = np.argmax(P[nT0-1, nTH:nT])
    # find the opt path
    #ik,jk,K = backtrack(bt,nT0-1,nTH+offIdx)
    #c = np.zeros(K)
    #for k in range(K):
    #    c[k] = C[ik[k],jk[k]]

    # warped X and per-frame similarity
    Xp,c = getWarped(bt,nT0-1,nTH+offIdx,X0,R,w,X,axis)

    '''
    print(np.array([ik,jk]))
    fig,ax = plt.subplots()
    im = plt.pcolormesh(P)
    plt.plot(jk,ik,'k')
    plt.autoscale(True,'both',True)
    fig.colorbar(im,ax=ax)
    plt.show()
    '''

    return Xp,c,p

def specFusion(T):
    N = len(T)
    pp = np.zeros(N)
    SS = np.empty(N,dtype=object)
    RR = np.empty(N,dtype=object)
    ww = np.empty(N,dtype=object)
    for n in range(N):
        S = np.array(T[n]) # do not modify, make a separate copy
        #print('np.shape(S) = '+str(np.shape(S)))
        #w = np.ones(np.shape(S)[1])
        w = np.max(S,0)
        R = proTFRegion(S)

        for ite in range(3):
            p = np.zeros(N)
            Xp = np.empty(N,dtype=object)
            c = np.empty(N,dtype=object)
            for m in range(N):
                Xp[m],c[m],p[m] = dtw(S,R,w,T[m],0)

            # reformat Xp
            eXp = np.concatenate(Xp).ravel().reshape(N,np.shape(S)[0],np.shape(S)[1])
            #print('np.shape(eXp) = '+str(np.shape(eXp)))
            # reformat c
            ec = np.concatenate(c).ravel().reshape(N,np.shape(S)[1])
            #print('np.shape(ec) = '+str(np.shape(ec)))

            for i in range(np.shape(eXp)[1]):
                for j in range(np.shape(eXp)[2]):
                    S[i,j] = np.median(eXp[:,i,j])
            w = getWeights(np.mean(ec,axis=0))
            R = proTFRegion(S)

        pp[n] = np.mean(p)
        SS[n] = S
        ww[n] = w
        RR[n] = R
    n_ast = np.argmax(pp)
    S_h = SS[n_ast]
    R_h = RR[n_ast]
    w_h = ww[n_ast]

    return S_h,R_h,w_h

def getWarped(bt,i0,j0,X0,R,w,X,axis):
    if axis == 0:
        Xp = np.zeros(np.shape(X0))
        i = i0
        j = j0
        while globalCon(bt,i,j):
            if bt[i,j] == 0:
                Xp[:,i] = X[:,j]
                i = i-1
                j = j-1
            elif bt[i,j] == 1:
                Xp[:,i] = (X[:,j]+X[:,j-1])*0.5
                i = i-1
                j = j-2
            elif bt[i,j] == 2:
                Xp[:,i] = X[:,j]
                Xp[:,i-1] = X[:,j]
                i = i-2
                j = j-1
            elif bt[i,j] == 3:
                Xp[:,i] = X0[:,i]
                i = i-1
                j = j
            else:
                break
    else:
        Xp = np.zeros(np.shape(X))
        i = i0
        j = j0
        while globalCon(bt,i,j):
            if bt[i,j] == 0:
                Xp[:,j] = X0[:,i]
                i = i-1
                j = j-1
            elif bt[i,j] == 1:
                Xp[:,j] = X0[:,i]
                Xp[:,j-1] = X0[:,i]
                i = i-1
                j = j-2
            elif bt[i,j] == 2:
                Xp[:,j] = (X0[:,i]+X0[:,i-1])*0.5
                i = i-2
                j = j-1
            elif bt[i,j] == 3:
                i = i-1
                j = j
            else:
                break


    nTp = np.shape(Xp)[1]
    c = np.zeros(nTp)
    for i in range(nTp):
        if axis == 0:
            u = X0[:,i]*R[:,i]
            v = Xp[:,i]*R[:,i]
        else:
            u = X[:,i]
            v = Xp[:,i]
        c[i] = cosSim(u,v)

    return Xp,c

def backtrack(bt,i0,j0):
    '''
    print('i0 = '+str(i0))
    print('j0 = '+str(j0))
    '''
    ik = []
    jk = []

    i = i0
    j = j0
    while globalCon(bt,i,j):
        ik.append(i)
        jk.append(j)

        if bt[i,j] == 0:
            i = i-1
            j = j-1
        elif bt[i,j] == 1:
            i = i-1
            j = j-2
        elif bt[i,j] == 2:
            i = i-2
            j = j-1
        elif bt[i,j] == 3:
            i = i-1
            j = j
        else:
            break

    return ik,jk,len(ik)

def cosSim(u,v):
    nu = np.linalg.norm(u)
    nv = np.linalg.norm(v)
    if nu == 0 or nv == 0:
        return 0
    return np.dot(u,v)/nu/nv

def globalCon(P,i,j):
    nT0,nT = np.shape(P)
    offset = int(np.floor(0.25*nT))
    slope = float(nT0)/nT
    retval = ((i <= offset+slope*j and i < nT0) and 
        (i >=0 and i >= slope*(j-offset)) and
        (j >= 0 and j <= nT))

    return retval
    
def getWeights(c):
    w = np.array(c)
    sw = np.sum(w) # sum of weights
    for i in range(len(w)):
        w[i] = w[i]/sw

    return w

def artItfExp(y,w,itf,fs,tBlk,tInc,dSIRs=range(-18,6,3),par=True,debug=0):
    # artificial interference
    # y: clean input
    # itf: interference

    # loop over desired SIR (signal interference ratio)
    if par:
        cs = Parallel(n_jobs=8)(delayed(artItfExpPar)(dSIR,y,w,itf,fs,tBlk,tInc,debug) for dSIR in dSIRs)
    else:
        cs = []
        for dSIR in dSIRs:
            cs.append(artItfExpPar(dSIR,y,w,itf,fs,tBlk,tInc,debug))

    return np.array(cs)

def artItfExpPar(dSIR,y,w,itf,fs,tBlk,tInc,debug=0):
    yS,ynS,ff,tt = audio_class.corrupt(y,itf,dSIR,fs,tBlk,tInc)
    wS,wnS,ff,tt = audio_class.corrupt(w,itf,dSIR,fs,tBlk,tInc)

    yX = specFusion([yS])
    ynX = specFusion([ynS])

    if debug >= 1:
        plt.figure(figsize=(9, 8), dpi= 80, facecolor='w', edgecolor='k')

        plt.subplot(221)
        im = plt.pcolormesh(tt,ff,np.sqrt(yX[0]))
        plt.autoscale(True,'both',True)
        plt.subplot(222)
        im = plt.pcolormesh(tt,ff,np.sqrt(yX[1]))
        plt.autoscale(True,'both',True)
        plt.subplot(223)
        im = plt.pcolormesh(tt,ff,np.sqrt(ynX[0]))
        plt.autoscale(True,'both',True)
        plt.subplot(224)
        im = plt.pcolormesh(tt,ff,np.sqrt(ynX[1]))
        plt.autoscale(True,'both',True)

        plt.show()

    # no need to create new obj
    # since these are read-only
    TemX =  [yX, yX,  ynX,ynX]
    X =     [wnS,ynS, wS, yS]
    N = len(TemX)

    Xp = [None]*N
    minDist = [None]*N
    c = [None]*N
    for k in range(N):
        Xp[k],c[k],minDist[k] = dtw(TemX[k][0],TemX[k][1],TemX[k][2],X[k],0)
        c[k] = audio_class.smooth(c[k])

    if debug >= 2:
        for k in range(N):
            plt.figure(figsize=(9, 8), dpi= 80, facecolor='w', edgecolor='k')

            plt.subplot(211)
            plt.pcolormesh(tt,ff,np.sqrt(X[k]))
            plt.autoscale(True,'both',True)
            plt.title('np.shape(X[%d]) = %s' % (k,np.shape(Xp[k])))
            plt.subplot(212)
            plt.plot(c[k])
            plt.axis([0,len(c[k]),0,1.1])
            #plt.autoscale(True,'both',True)

        plt.show()

    return c
